<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Json;

use Illuminate\Support\Facades\File;

class JSON
{
    public static function encode(array $data, int $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES): string
    {
        return json_encode($data, $flags);
    }

    public static function decode(string $json, bool $associative = true): array
    {
        return json_decode($json, $associative);
    }

    public static function decodeFile(string $file, bool $associative = true): array
    {
        return self::decode(File::get($file), $associative);
    }
}
