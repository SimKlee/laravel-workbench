<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Parser;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use PhpParser\Node;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\UnionType;
use PhpParser\Node\UseItem;
use PhpParser\NodeFinder;
use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;
use SimKlee\LaravelWorkbench\NodeVisitors\RemoveAttributes;

abstract class AbstractParser
{
    protected array      $ast = [];
    protected NodeFinder $nodeFinder;

    protected array $useMap = [];

    private bool $removeAttributes = true;

    public function __construct(protected $class)
    {
        if (is_object($this->class)) {
            $this->class = $this->class::class;
        }

        $this->nodeFinder = new NodeFinder();
        $this->ast        = (new ParserFactory())->createForHostVersion()
            ->parse(File::get(base_path($this->class2File($this->class))));

        if ($this->removeAttributes) {
            $traverser = new NodeTraverser();
            $traverser->addVisitor(new RemoveAttributes());
            $this->ast = $traverser->traverse($this->ast);
        }

        $this->useMap = $this->getUseMap();
    }

    private function getUseMap(): array
    {
        return $this->findInstanceOf(UseItem::class)
            ->mapWithKeys(
                fn(UseItem $useItem) => [
                    !is_null($useItem->alias)
                        ? $useItem->alias->name
                        : collect(explode('\\', $useItem->name->name))->last()
                    => $useItem->name->name,
                ])->toArray();
    }

    protected function findInstanceOf(string $class): Collection
    {
        return collect($this->nodeFinder->findInstanceOf($this->ast, $class));
    }

    protected function findFirstInstanceOf(string $class): ?Node
    {
        return $this->nodeFinder->findFirstInstanceOf($this->ast, $class);
    }

    public function getMethod(string $name): ClassMethod|null
    {
        return collect($this->nodeFinder->findInstanceOf($this->ast, ClassMethod::class))
            ->filter(fn(ClassMethod $classMethod) => $classMethod->name->name === $name)
            ->first();
    }

    public function getReturnTypes(ClassMethod $classMethod): array
    {
        return ($classMethod->returnType instanceof UnionType)
            ? $classMethod->returnType->types
            : [$classMethod->returnType];
    }

    protected function class2File(string $class): string
    {
        return collect(explode('\\', $class))
                ->map(function (string $part) {
                    return match ($part) {
                        'App'      => 'app',
                        'Database' => 'database',
                        'Seeders'  => 'seeders',
                        default    => $part,
                    };
                })->implode('/') . '.php';
    }
}
