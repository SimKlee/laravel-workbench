<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Parser;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use PhpParser\Node;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\Float_;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Enum_;
use PhpParser\Node\Stmt\Interface_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\Trait_;
use PhpParser\Node\Stmt\TraitUse;
use PhpParser\Node\Stmt\Use_;
use PhpParser\Node\UnionType;
use PhpParser\NodeAbstract;
use PhpParser\NodeFinder;
use PhpParser\ParserFactory;
use PhpParser\Parser;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelWorkbench\PhpObjects\ArrayItem as CustomArrayItem;
use SimKlee\LaravelWorkbench\Tools\FQN;

class PhpParser
{
    private Parser     $parser;
    private NodeFinder $finder;

    public ?Namespace_ $namespace = null;
    public ?Class_     $class     = null;
    public ?Enum_      $enum      = null;
    public ?Trait_     $trait     = null;
    public ?Interface_ $interface = null;

    public array $ast = [];

    public function __construct(private string $file)
    {
        $this->parser    = (new ParserFactory())->createForNewestSupportedVersion();
        $this->finder    = new NodeFinder();
        $this->ast       = $this->parser->parse(File::get($this->file));
        $this->namespace = $this->findFirstInstanceOf(Namespace_::class);
        $this->class     = $this->findFirstInstanceOf(Class_::class);
        $this->enum      = $this->findFirstInstanceOf(Enum_::class);
        $this->trait     = $this->findFirstInstanceOf(Trait_::class);
        $this->interface = $this->findFirstInstanceOf(Interface_::class);
    }

    public function addClassStmt(Node $stmt): void
    {
        $this->class->stmts[] = $stmt;
    }

    public function addNamespaceStmt(Node $stmt): void
    {
        $this->namespace->stmts[] = $stmt;
    }

    public function addEnumStmt(Node $stmt): void
    {
        $this->enum->stmts[] = $stmt;
    }

    public static function classOrTemplate(string $className, string $template): PhpParser
    {
        if (class_exists($className)) {
            return new PhpParser(self::fileFromFQN($className));
        }

        if (Str::contains($template, '\\')) {
            return new PhpParser(self::fileFromFQN($template));
        }

        return new PhpParser($template);
    }

    /**
     * @return Collection|Node[]
     */
    public function findInstanceOf(string $class, Node|array $ast = null): Collection
    {
        return collect($this->finder->findInstanceOf($ast ?? $this->ast, $class));
    }

    public function findFirstInstanceOf(string $class, Node|array $ast = null): ?Node
    {
        return $this->finder->findFirstInstanceOf($ast ?? $this->ast, $class);
    }

    public function findClassMethod(string $name, Node|array $ast = null): ?ClassMethod
    {
        return $this->findInstanceOf(ClassMethod::class, $ast ?? $this->class)
            ->filter(fn(ClassMethod $classMethod) => $classMethod->name->name === $name)
            ->first();
    }

    public function findProperty(string $name, Node|array $ast = null): ?Property
    {
        return $this->findInstanceOf(Property::class, $ast ?? $this->class)
            ->filter(fn(Property $property) => current($property->props)->name->name === $name)
            ->first();
    }

    public function findConstant(string $name, Node|array $ast = null): ?ClassConst
    {
        return $this->findInstanceOf(ClassConst::class, $ast ?? $this->class)
            ->filter(fn(ClassConst $classConst) => current($classConst->consts)->name->name === $name)
            ->first();
    }

    public static function fileFromFQN(string $fqn): string
    {
        return collect(explode('\\', $fqn))
            ->map(function (string $part, int $i) {
                if ($i === 0 && $part === 'App') {
                    return 'app';
                }

                return $part;
            })->implode('/');
    }

    public function sort(): void
    {
        $this->namespace->stmts = array_merge(
            collect($this->namespace->stmts)->filter(fn(NodeAbstract $stmt) => $stmt instanceof Use_)->toArray(),
            collect($this->namespace->stmts)->filter(fn(NodeAbstract $stmt) => $stmt instanceof Class_)->toArray(),
            collect($this->namespace->stmts)->filter(fn(NodeAbstract $stmt) => $stmt instanceof Enum_)->toArray(),
            collect($this->namespace->stmts)->filter(fn(NodeAbstract $stmt) => $stmt instanceof Trait_)->toArray(),
        );

        if (!is_null($this->class)) {
            $this->class->stmts = array_merge(
                collect($this->class->stmts)->filter(fn(Stmt $stmt) => $stmt instanceof TraitUse)->toArray(),
                collect($this->class->stmts)->filter(fn(Stmt $stmt) => $stmt instanceof ClassConst)->toArray(),
                collect($this->class->stmts)->filter(fn(Stmt $stmt) => $stmt instanceof Property)->toArray(),
                collect($this->class->stmts)->filter(fn(Stmt $stmt) => $stmt instanceof ClassMethod)->toArray(),
            );
        }

        if (!is_null($this->trait)) {
            $this->trait->stmts = array_merge(
                collect($this->trait->stmts)->filter(fn(Stmt $stmt) => $stmt instanceof TraitUse)->toArray(),
                collect($this->trait->stmts)->filter(fn(Stmt $stmt) => $stmt instanceof Property)->toArray(),
                collect($this->trait->stmts)->filter(fn(Stmt $stmt) => $stmt instanceof ClassMethod)->toArray(),
            );
        }
    }

    public function valueToObject($value): mixed
    {
        return match (gettype($value)) {
            'integer' => new Int_($value),
            'string'  => new String_($value),
            'double'  => new Float_($value),
            'boolean' => new ConstFetch(new Name($value ? 'true' : 'false')),
            'NULL'    => new ConstFetch(new Name('null')),
            'array'   => $this->arrayToObject($value),
            'object'  => match (get_class($value)) {
                default => $value,
            },
        };
    }

    public function createArrayItem($value, $key): ?ArrayItem
    {
        $value = $this->valueToObject($value);
        if ($value instanceof ArrayItem) {
            return $value;
        }
        $key = $this->valueToObject($key);

        return new ArrayItem($value, $key);
    }

    public function arrayToObject(array $array): ?Array_
    {
        return new Array_(
            items: collect($array)
                ->map(fn($value, $key) => $this->createArrayItem($value, $key))
                ->toArray()
        );
    }

    public function getTypes(array|string|null $types): UnionType|Identifier|Name|null
    {
        if (is_null($types)) {
            return null;
        }

        if (is_array($types)) {
            return new UnionType(
                collect($types)
                    ->map(function ($type) {
                        return $this->getType($type);
                    })->toArray()
            );
        }

        return $this->getType($types);
    }

    public function getType(string $type): Identifier|Name|null
    {
        if (Str::contains($type, '\\')) {
            return new Name(last(explode('\\', $type)));
        }

        return new Identifier($type);
    }

    public function write(string $file = null): bool
    {
        return File::put($file ?? $this->file, (new Standard())->prettyPrintFile($this->ast)) !== false;
    }
}
