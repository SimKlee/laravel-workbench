<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Node;
use PhpParser\Node\Identifier;
use PhpParser\Node\Stmt\Class_;

class ChangeClassName extends AbstractNodeVisitor
{
    public function __construct(private readonly string $className)
    {
    }

    public function leaveNode(Node $node): void
    {
        if ($node instanceof Class_) {
            $node->name = new Identifier($this->className);
        }
    }
}
