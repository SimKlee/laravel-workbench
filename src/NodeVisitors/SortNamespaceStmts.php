<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Node;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\TraitUse;

class SortNamespaceStmts extends AbstractNodeVisitor
{
    public function leaveNode(Node $node): void
    {
        if ($node instanceof Class_) {
            $node->stmts = array_merge(
                collect($node->stmts)->filter(fn (Stmt $stmt) => $stmt instanceof TraitUse)->toArray(),
                collect($node->stmts)->filter(fn (Stmt $stmt) => $stmt instanceof ClassConst)->toArray(),
                collect($node->stmts)->filter(fn (Stmt $stmt) => $stmt instanceof Property)->toArray(),
                collect($node->stmts)->filter(fn (Stmt $stmt) => $stmt instanceof ClassMethod)->toArray(),
            );
        }
    }
}
