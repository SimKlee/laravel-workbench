<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Node;

class RemoveAttributes extends AbstractNodeVisitor
{
    public function leaveNode(Node $node): void
    {
        $node->setAttributes([]);
    }
}
