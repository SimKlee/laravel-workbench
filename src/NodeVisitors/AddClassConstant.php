<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Modifiers;
use PhpParser\Node;
use PhpParser\Node\Const_;
use PhpParser\Node\Scalar\Float_;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\ClassConst;

class AddClassConstant extends AbstractNodeVisitor
{
    public function __construct(private readonly string $name, private readonly string|int|float $value, private readonly int $visibility = Modifiers::PUBLIC)
    {
    }

    public function leaveNode(Node $node): void
    {
        if ($node instanceof Node\Stmt\Class_) {
            $value = match (gettype($this->value)) {
                'string'  => new String_($this->value),
                'integer' => new Int_($this->value),
                'double'  => new Float_($this->value),
            };
            $const         = new ClassConst([new Const_($this->name, $value)], $this->visibility);
            $node->stmts[] = $const;
        }
    }
}
