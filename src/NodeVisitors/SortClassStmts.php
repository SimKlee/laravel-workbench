<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Node;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Use_;
use PhpParser\NodeAbstract;

class SortClassStmts extends AbstractNodeVisitor
{
    public function leaveNode(Node $node): void
    {
        if ($node instanceof Namespace_) {
            $node->stmts = array_merge(
                collect($node->stmts)->filter(fn (NodeAbstract $stmt) => $stmt instanceof Use_)->toArray(),
                collect($node->stmts)->filter(fn (NodeAbstract $stmt) => $stmt instanceof Class_)->toArray(),
            );
        }
    }
}
