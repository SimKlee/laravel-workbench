<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Node;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt\Namespace_;

class ChangeNamespace extends AbstractNodeVisitor
{
    public function __construct(private readonly string $namespace)
    {
    }

    public function leaveNode(Node $node): void
    {
        if ($node instanceof Namespace_) {
            $node->name = new Name($this->namespace);
        }
    }
}
