<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Node;
use PhpParser\Node\Stmt\ClassConst;

class ChangeModelTableConstant extends AbstractNodeVisitor
{
    public function __construct(private readonly string $table)
    {
    }

    public function leaveNode(Node $node): void
    {
        if ($node instanceof ClassConst) {
            $this->setConstValue($node, 'TABLE', $this->table);
        }
    }
}
