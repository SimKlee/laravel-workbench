<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Node;
use PhpParser\Node\Scalar\Float_;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\NodeVisitorAbstract;

abstract class AbstractNodeVisitor extends NodeVisitorAbstract
{
    protected function setConstValue(Node $node, string $name, $value): void
    {
        if ($node instanceof ClassConst) {
            $const = current($node->consts);
            if ($const->name->toString() === $name) {
                $const->value = match (gettype($value)) {
                    'string'  => new String_($value),
                    'integer' => new Int_($value),
                    'double'  => new Float_($value),
                };
            }
        }
    }
}
