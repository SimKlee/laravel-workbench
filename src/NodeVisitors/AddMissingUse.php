<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\NodeVisitors;

use PhpParser\Node;
use PhpParser\Node\Expr\ClassConstFetch;

class AddMissingUse extends AbstractNodeVisitor
{
    public function leaveNode(Node $node): void
    {
        if ($node instanceof ClassConstFetch) {

        }
    }
}
