<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench;

use Illuminate\Support\ServiceProvider;
use SimKlee\LaravelWorkbench\Console\Commands\AstCommand;
use SimKlee\LaravelWorkbench\Console\Commands\PintCommand;
use SimKlee\LaravelWorkbench\Console\Commands\ReadClassCommand;
use SimKlee\LaravelWorkbench\Console\Commands\ShowProjectPortsCommand;
use SimKlee\LaravelWorkbench\Console\Commands\SyncEnvFilesCommand;
use SimKlee\LaravelWorkbench\Console\Commands\TestInformationSchemaCommand;

class LaravelWorkbenchServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/laravel-workbench.php', 'laravel-workbench');
    }

    public function boot(): void
    {
        $this->commands([
            AstCommand::class,
            ReadClassCommand::class,
            PintCommand::class,
            SyncEnvFilesCommand::class,
            TestInformationSchemaCommand::class,
            ShowProjectPortsCommand::class,
        ]);

        if ($this->app->runningInConsole()) {

            $this->publishes(
                paths : [__DIR__ . '/../config/laravel-workbench.php' => config_path('laravel-workbench.php')],
                groups: ['laravel-workbench', 'config']
            );

        }
    }
}
