<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\String;

class StringBuffer
{
    public function __construct(private string $string = '')
    {
    }

    public static function create(string $string = ''): self
    {
        return new self($string);
    }

    public function append(string $string): self
    {
        $this->string .= $string;

        return $this;
    }

    public function appendIf(bool $condition, string $string, string $else = null): self
    {
        $this->string .= ($condition === true) ? $string : $else;

        return $this;
    }

    public function appendLine(string $string): self
    {
        $this->append($string . PHP_EOL);

        return $this;
    }

    public function prepend(string $string): self
    {
        $this->string .= $this->string . $string;

        return $this;
    }

    public function prependIf(bool $condition, string $string, string $else = null): self
    {
        $string       = ($condition === true) ? $string : $else;
        $this->string .= $this->string . $string;

        return $this;
    }

    public function prependLine(string $string): self
    {
        $this->prepend($string . PHP_EOL);

        return $this;
    }

    public function __toString(): string
    {
        return $this->string;
    }

    public function toString(): string
    {
        return $this->string;
    }
}
