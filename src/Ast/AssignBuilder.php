<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Modifiers;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Stmt\Expression;
use SimKlee\LaravelWorkbench\Ast\Traits\HasArgsTrait;

class AssignBuilder extends AbstractBuilder
{
    private Expr|Variable $var;
    private Expr          $expr;

    public static function create(Expr|Variable|string $var = null,
                                  Expr|AbstractBuilder   $expr = null): AssignBuilder
    {
        $instance = new static();

        if (!is_null($var)) {
            $instance->var($var);
        }
        if (!is_null($expr)) {
            $instance->expr($expr);
        }

        return $instance;
    }

    public function var(Expr|Variable|string $var): self
    {
        if (is_string($var)) {
            $var = new Variable($var);
        }

        $this->var = $var;

        return $this;
    }

    public function expr(Expr|AbstractBuilder $expr): self
    {
        if ($expr instanceof AbstractBuilder) {
            $expr = $expr->object();
        }

        $this->expr = $expr;

        return $this;
    }

    public function object(): Assign
    {
        return new Assign(
            var : $this->var,
            expr: $this->expr
        );
    }

    public function asExpression(): Expression
    {
        return new Expression($this->object());
    }
}
