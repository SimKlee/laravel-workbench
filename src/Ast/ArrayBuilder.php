<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use Illuminate\Support\Collection;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr\Array_;

class ArrayBuilder extends AbstractBuilder
{
    private array $items;

    public static function create(Collection|array $items = []): ArrayBuilder
    {
        return (new static())->items($items);
    }

    public function item($item): self
    {
        $this->items[] = $item instanceof AbstractBuilder ? $item->object() : $item;

        return $this;
    }

    public function items(Collection|array $items): self
    {
        if (is_array($items)) {
            $items = collect($items);
        }

        $items->each(fn($item) => $this->item($item));

        return $this;
    }

    public function object(): Array_
    {
        return new Array_($this->items);
    }

    public function asArg(): Arg
    {
        return new Arg($this->object());
    }
}
