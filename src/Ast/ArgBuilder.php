<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Arg;
use PhpParser\Node\Expr;

class ArgBuilder extends AbstractBuilder
{
    private ?Expr $value = null;

    public static function create(AbstractBuilder|Expr $value = null): ArgBuilder
    {
        $instance = new static();

        if (!is_null($value)) {
            $instance->value($value);
        }

        return $instance;
    }

    public function value(AbstractBuilder|Expr $value): self
    {
        $this->value = $value instanceof AbstractBuilder ? $value->object() : $value;

        return $this;
    }

    public function object(): Arg
    {
        return new Arg($this->value);
    }
}
