<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Modifiers;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Stmt\Expression;
use SimKlee\LaravelWorkbench\Ast\Traits\HasArgsTrait;

class VariableBuilder extends AbstractBuilder
{
    private ?string $name = null;

    public static function create(string $name = null): VariableBuilder
    {
        $instance = new static();

        if (!is_null($name)) {
            $instance->name($name);
        }

        return $instance;
    }

    public function name(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function object(): Variable
    {
        return new Variable($this->name);
    }

    public function asArg(): Arg
    {
        return new Arg($this->object());
    }
}
