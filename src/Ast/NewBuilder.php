<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Expr\New_;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt\Expression;
use SimKlee\LaravelWorkbench\Ast\Traits\HasArgsTrait;

class NewBuilder extends AbstractBuilder
{
    use HasArgsTrait;

    private ?Name $class = null;

    public static function create(Name|string $class = null, array $args = []): NewBuilder
    {
        $instance = new static();

        if (!is_null($class)) {
            $instance->class($class);
        }

        $instance->args($args);

        return $instance;
    }

    public function class(Name|string $class): self
    {
        if (is_string($class)) {
            $class = new Name($class);
        }

        $this->class = $class;

        return $this;
    }

    public function object(): New_
    {
        return new New_($this->class);
    }

    public function asExpression(): Expression
    {
        return new Expression($this->object());
    }
}
