<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Arg;
use PhpParser\Node\ComplexType;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Param;

class ParamBuilder extends AbstractBuilder
{
    private ?Expr                            $var      = null;
    private ?Expr                            $default  = null;
    private bool                             $byRef    = false;
    private bool                             $variadic = false;
    private Identifier|Name|ComplexType|null $type     = null;

    public static function create(Expr|string $var = null, Identifier|Name|ComplexType|string $type = null): ParamBuilder
    {
        $instance = new static();

        if (!is_null($var)) {
            $instance->var($var);
        }
        if (!is_null($type)) {
            $instance->type($type);
        }

        return $instance;
    }

    public function var(Expr|string $var): self
    {
        if (is_string($var)) {
            $var = new Variable($var);
        }

        $this->var = $var;

        return $this;
    }

    public function type(Identifier|Name|ComplexType|string $type): self
    {
        if (is_string($type)) {
            $type = new Name($type);
        }

        $this->type = $type;

        return $this;
    }

    public function byRef(): self
    {
        $this->byRef = true;

        return $this;
    }

    public function object(): Param
    {
        return new Param(
            var     : $this->var,
            default : $this->default,
            type    : $this->type,
            byRef   : $this->byRef,
            variadic: $this->variadic
        );
    }
}
