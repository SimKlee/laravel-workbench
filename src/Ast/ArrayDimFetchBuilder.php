<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Expr;
use PhpParser\Node\Expr\ArrayDimFetch;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Scalar\String_;

class ArrayDimFetchBuilder extends AbstractBuilder
{
    private ?Variable $var = null;
    private ?Expr     $dim = null;

    public static function create(Variable|string $var = null, Expr|AbstractBuilder|string $dim = null): ArrayDimFetchBuilder
    {
        $instance = new static();

        if (!is_null($var)) {
            $instance->var($var);
        }
        if (!is_null($dim)) {
            $instance->dim($dim);
        }

        return $instance;
    }

    public function var(Variable|string $var): self
    {
        if (is_string($var)) {
            $var = new Variable($var);
        }

        $this->var = $var;

        return $this;
    }

    public function dim(AbstractBuilder|Expr|string $dim): self
    {
        if ($dim instanceof AbstractBuilder) {
            $dim = $dim->object();
        }

        if (is_string($dim)) {
            $dim = new String_($dim);
        }

        $this->dim = $dim;

        return $this;
    }

    public function object(): ArrayDimFetchBuilder
    {
        return new ArrayDimFetch(var: $this->var, dim: $this->dim);
    }
}
