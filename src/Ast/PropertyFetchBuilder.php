<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Expr\PropertyFetch;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;

class PropertyFetchBuilder extends AbstractBuilder
{
    private ?Variable   $var  = null;
    private ?Identifier $name = null;

    public static function create(Variable|string $var = null, Identifier|string $name = null): PropertyFetchBuilder
    {
        $instance = new static();

        if (!is_null($var)) {
            $instance->var($var);
        }

        if (!is_null($name)) {
            $instance->name($name);
        }

        return $instance;
    }


    public function var(Variable|string $var): self
    {
        if (is_string($var)) {
            $var = new Variable($var);
        }

        $this->var = $var;

        return $this;
    }

    public function name(Identifier|string $name): self
    {
        if (is_string($name)) {
            $name = new Identifier($name);
        }

        $this->name = $name;

        return $this;
    }

    public function object(): PropertyFetch
    {
        return new PropertyFetch(var: $this->var, name: $this->name);
    }
}
