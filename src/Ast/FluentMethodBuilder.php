<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Identifier;

class FluentMethodBuilder
{
    public MethodCall $methodCall;

    public function __construct(MethodCall $methodCall)
    {
        $this->methodCall = $methodCall;
    }

    public static function create(MethodCall $methodCall): self
    {
        return new self($methodCall);
    }

    public function addMethod(string $name, array $args = []): self
    {
        $this->methodCall = new MethodCall(
            var : $this->methodCall,
            name: new Identifier(name: $name),
            args: $args
        );

        return $this;
    }
}
