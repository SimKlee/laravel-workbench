<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Arg;
use PhpParser\Node\Expr;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt\Use_;
use PhpParser\Node\UseItem;

class UseBuilder extends AbstractBuilder
{
    protected string $class;
    protected ?string $alias = null;

    public static function create(string $class = null, string $alias = null): UseBuilder
    {
        $instance = new static();

        if (!is_null($class)) {
            $instance->class($class);
        }

        if (!is_null($alias)) {
            $instance->alias($alias);
        }

        return $instance;
    }

    public function class(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function alias(string $alias = null): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function object(): Use_
    {
        return new Use_(uses: [new UseItem(name: new Name($this->class), alias: $this->alias)]);
    }
}
