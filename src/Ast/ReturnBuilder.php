<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Expr;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\Return_;

class ReturnBuilder extends AbstractBuilder
{
    public function __construct(protected Expr $expr)
    {
    }

    public static function create(AbstractBuilder|Expr $expr = null): self
    {
        if ($expr instanceof AbstractBuilder) {
            $expr = $expr->object();
        }

        return new static($expr);
    }

    public function classConstFetch(Name|string $class, Identifier|string $name): self
    {
        $this->expr = ClassConstFetchBuilder::create(class: $class, name: $name)->object();

        return $this;
    }

    public function string(string $string): self
    {
        $this->expr = new String_($string);

        return $this;
    }

    public function object(): Return_
    {
        return new Return_($this->expr);
    }
}
