<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Name;

class ConstFetchBuilder extends AbstractBuilder
{
    private ?Name $name = null;

    public static function create(Name|string $name = null): ConstFetchBuilder
    {
        $instance = new static();

        if (!is_null($name)) {
            $instance->name($name);
        }

        return $instance;
    }

    public function name(Name|string $name): self
    {
        $this->name = is_string($name) ? new Name($name) : $name;

        return $this;
    }

    public function object(): ConstFetch
    {
        return new ConstFetch($this->name);
    }
}
