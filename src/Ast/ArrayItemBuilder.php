<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr;

class ArrayItemBuilder extends AbstractBuilder
{
    private ?Expr $value = null;
    private ?Expr $key   = null;

    public static function create(AbstractBuilder|Expr $value = null, AbstractBuilder|Expr $key = null): ArrayItemBuilder
    {
        $instance = new static();

        if (!is_null($value)) {
            $instance->value($value);
        }

        if (!is_null($key)) {
            $instance->key($key);
        }

        return $instance;
    }

    public function value(AbstractBuilder|Expr $value): self
    {
        $this->value = $value instanceof AbstractBuilder ? $value->object() : $value;

        return $this;
    }

    public function key(AbstractBuilder|Expr $key): self
    {
        $this->key = $key instanceof AbstractBuilder ? $key->object() : $key;

        return $this;
    }

    public function object(): ArrayItem
    {
        return new ArrayItem($this->value, $this->key);
    }
}
