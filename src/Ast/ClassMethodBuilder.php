<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\ComplexType;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Param;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\UnionType;
use SimKlee\LaravelWorkbench\Ast\Traits\HasFlagsTrait;

class ClassMethodBuilder extends AbstractBuilder
{
    use HasFlagsTrait;

    private ?Identifier                             $name        = null;
    private Identifier|Name|ComplexType|string|null $returnType  = null;
    private array                                   $stmts       = [];
    private array                                   $params      = [];
    private array                                   $returnTypes = [];

    public static function create(Identifier|string|null $name = null): ClassMethodBuilder
    {
        $instance = new static();

        if (!is_null($name)) {
            $instance->name($name);
        }

        return $instance;
    }

    public static function createPublic(Identifier|string|null $name = null): ClassMethodBuilder
    {
        return self::create($name)->public();
    }

    public static function createProtected(Identifier|string|null $name = null): ClassMethodBuilder
    {
        return self::create($name)->protected();
    }

    public static function createPrivate(Identifier|string|null $name = null): ClassMethodBuilder
    {
        return self::create($name)->private();
    }

    public function name(Identifier|string $name): self
    {
        $this->name = is_string($name) ? new Identifier($name) : $name;

        return $this;
    }

    public function returnType(Name|Identifier|string $name): self
    {
        if ($name === 'void') {
            $name = new Identifier('void');
        }

        if (is_string($name)) {
            $name = new Name($name);
        }

        if (!in_array($name, $this->returnTypes)) {
            $this->returnTypes[] = $name;
        }

        return $this;
    }

    public function stmt(AbstractBuilder|Stmt $stmt): self
    {
        if ($stmt instanceof AbstractBuilder) {
            $stmt = $stmt->object();
        }

        $this->stmts[] = $stmt;

        return $this;
    }

    public function stmts(array $stmts): self
    {
        collect($stmts)->each(fn($stmt) => $this->stmt($stmt));

        return $this;
    }

    public function param(string $name, ?string $type = null, mixed $default = null): self
    {
        $this->params[$name] = [
            'type'    => $type,
            'default' => $default,
        ];

        return $this;
    }

    public function object(): ClassMethod
    {
        if (count($this->returnTypes) === 1) {
            $retType    = current($this->returnTypes);
            $returnType = is_string($retType) ? new Name($retType) : $retType;
        } else {
            $returnType = new UnionType(
                types: collect($this->returnTypes)
                    ->map(fn(string $retType) => new Name($retType))
                    ->toArray()
            );
        }

        return new ClassMethod(name: $this->name, subNodes: [
            'flags'      => $this->flags,
            'returnType' => $returnType,
            'stmts'      => $this->stmts,
            'params'     => collect($this->params)
                ->map(fn(array $info, string $name) => new Param(
                    var    : new Variable($name),
                    default: $info['default'] === 'null' ? ConstFetchBuilder::create(name: 'null')->object() : null,
                    type   : new Identifier($info['type'])
                ))->toArray(),
        ]);
    }
}
