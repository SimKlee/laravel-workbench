<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use SimKlee\LaravelWorkbench\Ast\Traits\HasArgsTrait;

class StaticCallBuilder extends AbstractBuilder
{
    use HasArgsTrait;

    private ?Name       $class = null;
    private ?Identifier $name  = null;

    public static function create(Name|string $class = null,
                                  Identifier|string $name = null,
                                  array $args = []): StaticCallBuilder
    {
        $instance = new static();

        if (!is_null($class)) {
            $instance->class($class);
        }

        if (!is_null($name)) {
            $instance->name($name);
        }

        $instance->args($args);

        return $instance;
    }

    public function class(Name|string $class): self
    {
        $this->class = is_string($class) ? new Name($class) : $class;

        return $this;
    }

    public function name(Identifier|string $name): self
    {
        $this->name = is_string($name) ? new Identifier($name) : $name;

        return $this;
    }

    public function object(): StaticCall
    {
        return new StaticCall($this->class, $this->name, $this->args);
    }
}
