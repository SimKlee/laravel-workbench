<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Modifiers;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Stmt\Expression;
use SimKlee\LaravelWorkbench\Ast\Traits\HasArgsTrait;

class MethodCallBuilder extends AbstractBuilder
{
    use HasArgsTrait;

    private ?Expr       $var  = null;
    private ?Identifier $name = null;

    public static function create(AbstractBuilder|Expr|string $var = null,
                                  Identifier|string           $name = null,
                                  array                       $args = []): MethodCallBuilder
    {
        $instance = new static();

        if (!is_null($var)) {
            $instance->var($var);
        }

        if (!is_null($name)) {
            $instance->name($name);
        }

        $instance->args($args);

        return $instance;
    }

    public function var(AbstractBuilder|Expr|string $expr): self
    {
        if (is_string($expr)) {
            $expr = new Variable($expr);
        }

        $this->var = $expr instanceof AbstractBuilder ? $expr->object() : $expr;

        return $this;
    }

    public function name(Identifier|string $name): self
    {
        $this->name = is_string($name) ? new Identifier($name) : $name;

        return $this;
    }

    public function object(): MethodCall
    {
        return new MethodCall($this->var, $this->name, $this->args);
    }

    public function asExpression(): Expression
    {
        return new Expression($this->object());
    }
}
