<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Arg;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use SimKlee\LaravelWorkbench\Ast\Traits\HasArgsTrait;

class ClassConstFetchBuilder extends AbstractBuilder
{
    private ?Name       $class = null;
    private ?Identifier $name  = null;

    public static function create(Name|string       $class = null,
                                  Identifier|string $name = null): ClassConstFetchBuilder
    {
        $instance = new static();

        if (!is_null($class)) {
            $instance->class($class);
        }

        if (!is_null($name)) {
            $instance->name($name);
        }

        return $instance;
    }

    public function class(Name|string $class): self
    {
        $this->class = is_string($class) ? new Name($class) : $class;

        return $this;
    }

    public function name(Identifier|string $name): self
    {
        $this->name = is_string($name) ? new Identifier($name) : $name;

        return $this;
    }

    public function object(): ClassConstFetch
    {
        return new ClassConstFetch($this->class, $this->name);
    }

    public function asArg(): Arg
    {
        return new Arg($this->object());
    }
}
