<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Name;

abstract class AbstractBuilder
{
    public static function create(): mixed
    {
        return new static();
    }

    abstract public function object(): mixed;
}
