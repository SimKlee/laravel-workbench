<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast\Traits;

use PhpParser\Node\Expr\Variable;

trait HasVarTrait
{
    protected Variable|null $var = null;

    public function var(Variable|string $var): self
    {
        if (is_string($var)) {
            $var = new Variable($var);
        }

        $this->var = $var;

        return $this;
    }
}
