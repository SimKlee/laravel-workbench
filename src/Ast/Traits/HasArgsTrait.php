<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast\Traits;

use PhpParser\Node\Arg;
use PhpParser\Node\Expr;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use SimKlee\LaravelWorkbench\Ast\AbstractBuilder;

trait HasArgsTrait
{
    protected array $args = [];

    public function arg(AbstractBuilder|Arg|Expr $arg): static
    {
        if ($arg instanceof AbstractBuilder) {
            $arg = $arg->object();
        }

        if ($arg instanceof Expr) {
            $arg = new Arg($arg);
        }

        $this->args[] = $arg;

        return $this;
    }

    public function args(array $args): static
    {
        collect($args)->each(fn($arg) => $this->arg($arg));

        return $this;
    }

    public function stringArg(string $arg): static
    {
        return $this->arg(new String_($arg));
    }

    public function intArg(int $arg): static
    {
        return $this->arg(new Int_($arg));
    }

}
