<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast\Traits;

use PhpParser\Node\Identifier;

trait HasNameTrait
{
    protected Identifier|null $name = null;

    public function name(Identifier|string $name): self
    {
        if (is_string($name)) {
            $name = new Identifier($name);
        }

        $this->name = $name;

        return $this;
    }
}
