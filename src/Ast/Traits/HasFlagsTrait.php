<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast\Traits;

use PhpParser\Modifiers;

trait HasFlagsTrait
{
    protected int $flags = 0;

    public function flags(int $flags): self
    {
        $this->flags = $flags;

        return $this;
    }

    public function public(): self
    {
        return $this->flags(Modifiers::PUBLIC);
    }

    public function private(): self
    {
        return $this->flags(Modifiers::PRIVATE);
    }

    public function protected(): self
    {
        return $this->flags(Modifiers::PROTECTED);
    }
}
