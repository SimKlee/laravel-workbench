<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Ast;

use PhpParser\Node\Arg;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use SimKlee\LaravelWorkbench\Ast\Traits\HasArgsTrait;

class FuncCallBuilder extends AbstractBuilder
{
    use HasArgsTrait;

    private ?Name $name = null;

    public static function create(Name|string $name = null,
                                  array       $args = []): static
    {
        $instance = new static();

        if (!is_null($name)) {
            $instance->name($name);
        }

        $instance->args($args);

        return $instance;
    }

    public function name(Name|string $name): self
    {
        $this->name = is_string($name) ? new Name($name) : $name;

        return $this;
    }

    public function object(): FuncCall
    {
        return new FuncCall(name: $this->name, args: $this->args);
    }
}
