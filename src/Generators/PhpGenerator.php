<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Illuminate\Support\Facades\File;
use PhpParser\Node;
use PhpParser\NodeFinder;
use PhpParser\Parser;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;

class PhpGenerator
{
    public Parser     $parser;
    public NodeFinder $finder;
    public array      $ast = [];

    public function __construct(private readonly string $file)
    {
        $this->parser = (new ParserFactory())->createForNewestSupportedVersion();
        $this->finder = new NodeFinder();
        $this->ast    = $this->parser->parse(File::get($this->file));
    }

    public function findInstanceOf(string $class): array
    {
        return $this->finder->findInstanceOf($this->ast, $class);
    }

    public function findFirstInstanceOf(string $class): Node
    {
        return $this->finder->findFirstInstanceOf($this->ast, $class);
    }

    public function write(string $file = null, bool $overwrite = true): bool
    {
        $file = $this->file;

        if ($overwrite === false && File::exists($file)) {
            return false;
        }

        $parts = explode('/', $file);
        array_pop($parts);
        $path = implode('/', $parts);
        if (File::isDirectory($path) === false) {
            File::makeDirectory(path: $path, recursive: true);
        }

        return File::put($file, (new Standard())->prettyPrintFile($this->ast)) !== false;
    }
}
