<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Exception;
use Illuminate\Support\Facades\File;
use PhpParser\Comment\Doc;
use PhpParser\Node\DeclareItem;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Declare_;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\TraitUse;
use PhpParser\Node\Stmt\Use_;
use PhpParser\NodeAbstract;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelWorkbench\Generators\Components\AnnotationComponent;
use SimKlee\LaravelWorkbench\Generators\Components\ConstantComponent;
use SimKlee\LaravelWorkbench\Generators\Components\MethodComponent;
use SimKlee\LaravelWorkbench\Generators\Components\PropertyComponent;
use SimKlee\LaravelWorkbench\Generators\Components\TraitComponent;
use SimKlee\LaravelWorkbench\Generators\Components\UseComponent;
use SimKlee\LaravelWorkbench\Parser\PhpParser;
use SimKlee\LaravelWorkbench\Tools\FQN;

class ClassGenerator
{
    protected PhpParser        $parser;
    public UseComponent        $useComponent;
    public TraitComponent      $traitComponent;
    public PropertyComponent   $propertyComponent;
    public MethodComponent     $methodComponent;
    public ConstantComponent   $constantComponent;
    public AnnotationComponent $annotationComponent;

    /**
     * @throws Exception
     */
    public function __construct(protected string $class, string $templateFile = null)
    {
        $file = $this->getFile();
        if (File::exists($file) === false && is_null($templateFile)) {
            throw new Exception(sprintf('Class %s does not exists. You must set a template file!', $this->class));
        }

        $file                      = File::exists($file) ? $file : $templateFile;
        $this->parser              = new PhpParser($file);
        $this->useComponent        = new UseComponent($this->parser);
        $this->traitComponent      = new TraitComponent($this->parser, $this->useComponent);
        $this->propertyComponent   = new PropertyComponent($this->parser);
        $this->methodComponent     = new MethodComponent($this->parser);
        $this->constantComponent   = new ConstantComponent($this->parser);
        $this->annotationComponent = new AnnotationComponent($this->parser);

        $this->init();
    }

    protected function init(): void
    {
        if (!empty($this->class)) {
            $this->setClassName(FQN::className($this->class));
            $this->setNamespace(FQN::namespace($this->class));
        }
    }

    public function setClassComment(string $comment): self
    {
        $this->parser->class->setAttribute('comments', [new Doc($comment)]);

        return $this;
    }

    public function setClassName(string $name): self
    {
        $this->parser->class->name = new Identifier(name: $name);

        return $this;
    }

    public function setInterfaceName(string $name): self
    {
        if ($this->parser->interface) {
            $this->parser->interface->name = new Identifier(name: $name);
        }

        return $this;
    }

    public function addInterface(string $interface): self
    {
        if ($this->hasInterface($interface) === false) {
            $this->useComponent->add($interface);
            $this->parser->class->implements[] = new Name(class_basename($interface));
        }

        return $this;
    }

    public function hasInterface(string $interface): bool
    {
        return collect($this->parser->class->implements)
            ->filter(fn(Name $name) => $name->name === class_basename($interface))
            ->isNotEmpty();
    }

    public function setNamespace(string $namespace): self
    {
        $this->parser->namespace->name = new Name(name: $namespace);

        return $this;
    }

    public function setExtends(string $class): self
    {
        $this->useComponent->add($class);
        $this->parser->class->extends = new Name(FQN::className($class));

        return $this;
    }

    public function extends(): ?string
    {
        return $this->parser->class->extends->name ?? null;
    }

    public function declareStrictTypes(): self
    {
        $notExists = $this->parser->findInstanceOf(Declare_::class, $this->parser->namespace)
            ->filter(fn(Declare_ $declare) => current($declare->declares)->key->name === 'strict_types')
            ->isEmpty();

        if ($notExists) {
            $this->parser->namespace->stmts[] = new Declare_(declares: [
                new DeclareItem(key: new Identifier(name: 'strict_types'), value: new Int_(value: 1)),
            ]);
        }

        return $this;
    }

    protected function beforeWrite(): void
    {
    }

    public function write(string $file = null, bool $overwrite = true): bool
    {
        if (is_null($file)) {
            $file = $this->getFile();
        }

        if ($overwrite === false && File::exists($file)) {
            return false;
        }

        $this->beforeWrite();

        if ($this->annotationComponent->isNotEmpty()) {
            $this->setClassComment($this->annotationComponent->toString());
        }

        $this->parser->sort();

        $parts = explode('/', $file);
        array_pop($parts);
        $path = implode('/', $parts);
        if (File::isDirectory($path) === false) {
            File::makeDirectory(path: $path, recursive: true);
        }

        return File::put($file, (new Standard())->prettyPrintFile($this->parser->ast)) !== false;
    }

    public function addUses(array $uses): self
    {
        collect($uses)->each(fn(string $class) => $this->useComponent->add($class));

        return $this;
    }

    public function getFile(): string
    {
        return FQN::file($this->class);
    }

    public function exists(): bool
    {
        return File::exists($this->getFile());
    }

    public function getClass(): string
    {
        return $this->class;
    }
}
