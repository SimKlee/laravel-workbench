<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use PhpParser\Node\PropertyItem;
use PhpParser\Node\VarLikeIdentifier;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;
use SimKlee\LaravelWorkbench\Builder\ModelDefinitionBuilder;
use SimKlee\LaravelWorkbench\Builder\RelationDefinitionBuilder;
use SimKlee\LaravelWorkbench\PhpObjects\ArrayItem;
use SimKlee\LaravelWorkbench\PhpObjects\Constant;

class ModelGenerator extends ClassGenerator
{
    protected ModelDefinition        $modelDefinition;
    protected ModelDefinitionBuilder $modelBuilder;
    protected array                  $annotations = [];

    public function __construct(protected string                             $file,
                                protected readonly ModelDefinitionCollection $modelDefinitions,
                                protected readonly string                    $modelName)
    {
        $this->modelDefinition = $this->modelDefinitions->get($this->modelName);
        $this->modelBuilder    = new ModelDefinitionBuilder($this->modelDefinition);

        parent::__construct($file, __DIR__ . '/../Templates/ModelTemplate.php');
    }

    /**
     * @throws \Exception
     */
    public static function fromModelTemplate(ModelDefinitionCollection $modelDefinitions, string $modelName, string $file): self
    {
        return new self($file, $modelDefinitions, $modelName);
    }

    /**
     * @throws \Exception
     */
    public static function fromModel(ModelDefinitionCollection $modelDefinitions, Model $model): self
    {
        $parts = explode('\\', $model::class);
        array_shift($parts); // remove App
        $modelName = array_pop($parts);
        $path      = implode('/', $parts);
        $file      = app_path(sprintf('%s/%s.php', $path, $modelName));

        return new self($file, $modelDefinitions, $modelName);
    }

    public function generate(?string $file = null): int|false
    {
        if (is_null($file)) {
            $file = sprintf('App\Models\%s', $this->modelDefinition->model());
        }

        $this->parser->sort();

        $written = File::put($file, (new Standard())->prettyPrintFile($this->parser->ast));

        Artisan::call('pint', ['file' => $file]);

        return $written;
    }

    protected function init(): void
    {
        $this->setClassName($this->modelDefinition->model());
        $this->setNamespace(config('laravel-workbench.model.namespace'));
        $this->setExtends(config('laravel-workbench.model.extends'));

        if ($this->modelDefinition->columns->dates()->count() > 0) {
            $this->useComponent->add(Carbon::class);
        }

        $this->modelDefinition->interfaces()
            ->each(fn(string $interface) => $this->addInterface($interface));

        $this->modelDefinition->columns->enumerationClasses()
            ->each(fn(string $enumeration) => $this->useComponent->add($enumeration));

        if ($this->modelDefinition->softDelete()) {
            $this->traitComponent->add(SoftDeletes::class);
        }

        $this->processConstants();
        $this->processProperties();
        $this->processRelations();
        $this->processMethods();

        $this->setClassComment("/**\n" . implode(PHP_EOL, $this->annotations) . "\n */");
    }

    private function processConstants(): void
    {
        $tableConst                         = $this->constantComponent->get('TABLE');
        current($tableConst->consts)->value = $this->parser->valueToObject($this->modelDefinition->table());

        $this->modelDefinition->columns->all()
            ->each(function (ColumnDefinition $columnDefinition) {
                $this->annotations[] = sprintf(
                    ' * @property %s $%s',
                    $columnDefinition->dataType()->getCastType(),
                    $columnDefinition->name()
                );

                $this->constantComponent->add(
                    name : sprintf('PROPERTY_%s', Str::upper($columnDefinition->name())),
                    value: $columnDefinition->name()
                );
            });
    }

    private function processProperties(): void
    {
        $this->propertyComponent->get('timestamps')->props = [
            new PropertyItem(
                name   : new VarLikeIdentifier('timestamps'),
                default: $this->modelDefinition->timestamps()
            ),
        ];

        $this->propertyComponent->get('guarded')->props = [
            new ArrayItem(new Constant([$this->modelDefinition->model(), 'PROPERTY_ID'])),
        ];

        $this->propertyComponent->get('hidden')->props = [
            new ArrayItem(new Constant([$this->modelDefinition->model(), 'PROPERTY_ID'])),
        ];

        $defaults = $this->modelDefinition->columns->all()
            ->filter(fn(ColumnDefinition $columnDefinition) => !is_null($columnDefinition->default()))
            ->map(fn(ColumnDefinition $columnDefinition) => new ArrayItem(
                value: $columnDefinition->default(),
                key  : new Constant([$this->modelDefinition->model(), sprintf('PROPERTY_%s', Str::upper($columnDefinition->name()))])
            ))->toArray();

        $this->propertyComponent->get('attributes')->props = [$defaults];


        if (config('laravel-workbench.model.use_casts_property')) {
            $this->parser->addClassStmt($this->modelBuilder->getCastsProperty());
        }
    }

    private function processMethods(): void
    {
        if (config('laravel-workbench.model.use_casts_method')) {
            $this->parser->addClassStmt($this->modelBuilder->getCastsMethod());
        }
    }

    private function processRelations(): void
    {
        $this->modelDefinition->relations->all()
            ->each(function (RelationDefinition $relationDefinition) {
                $builder = new RelationDefinitionBuilder($relationDefinition);
                $builder->getUses()->each(fn(string $use) => $this->useComponent->add($use));
                $builder->getConstants()->each(fn(string $value, string $name) => $this->constantComponent->add(name: $name, value: $value));
                $this->annotations[] = $builder->getModelRelationMethodAnnotation();
                $this->parser->addClassStmt($builder->getModelRelationMethod());
            });
    }
}
