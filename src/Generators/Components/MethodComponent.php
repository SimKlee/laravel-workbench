<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components;

use PhpParser\Modifiers;
use PhpParser\Node\ComplexType;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Param;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\ClassMethod;

class MethodComponent extends AbstractComponent
{
    public function has(string $name): bool
    {
        return $this->parser->findInstanceOf(ClassMethod::class, $this->base())
            ->filter(fn(ClassMethod $classMethod) => $classMethod->name->name === $name)
            ->isNotEmpty();
    }

    /**
     * @param string                           $name
     * @param Param[]                          $params
     * @param int                              $modifier
     * @param Identifier|Name|ComplexType|null $returnType
     * @param array                            $stmts
     * @return bool
     */
    public function add(string                           $name,
                        array                            $params = [],
                        int                              $modifier = Modifiers::PUBLIC,
                        null|Identifier|Name|ComplexType $returnType = new Identifier(name: 'void'),
                        array                            $stmts = []): bool
    {
        if ($this->has($name)) {
            return false;
        }

        $this->class->stmts[] = new ClassMethod(
            name    : new Identifier($name),
            subNodes: [
                'flags'      => $modifier,
                'params'     => $params,
                'returnType' => $returnType,
                'stmts'      => $stmts,
            ],
        );

        return true;
    }

    public function addClassMethod(ClassMethod $method): bool
    {
        if ($this->has($method->name->name)) {
            return false;
        }

        $this->base()->stmts[] = $method;

        return true;
    }

    public function remove(string $name): bool
    {
        if ($this->has($name) === false) {
            return false;
        }

        $this->base()->stmts[] = collect($this->base()->stmts)
            ->reject(fn(Stmt $stmt) => $stmt instanceof ClassMethod && $stmt->name->name === $name)
            ->toArray();

        return true;
    }

    public function get(string $name): ClassMethod|null
    {
        return $this->parser->findClassMethod($name, $this->base());
    }
}
