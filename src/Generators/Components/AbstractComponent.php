<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components;

use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Trait_;
use PhpParser\NodeFinder;
use SimKlee\LaravelWorkbench\Parser\PhpParser;

abstract class AbstractComponent
{
    public const BASE_CLASS = 'class';
    public const BASE_TRAIT = 'trait';

    protected NodeFinder $finder;
    public ?Namespace_   $namespace;
    public ?Class_       $class;
    public ?Trait_       $trait;

    public function __construct(protected PhpParser $parser, protected string $base = self::BASE_CLASS)
    {
        $this->finder    = new NodeFinder();
        $this->namespace = $this->parser->namespace;
        $this->class     = $this->parser->class;
        $this->trait     = $this->parser->trait;
    }

    protected function base(): Class_|Trait_
    {
        return $this->base === 'trait'
            ? $this->trait
            : $this->class;
    }
}
