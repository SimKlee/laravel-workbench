<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components;

use Illuminate\Support\Collection;
use SimKlee\LaravelWorkbench\Generators\Components\Annotations\AbstractAnnotation;
use SimKlee\LaravelWorkbench\Generators\Components\Annotations\MethodAnnotation;
use SimKlee\LaravelWorkbench\Generators\Components\Annotations\PropertyAnnotation;
use SimKlee\LaravelWorkbench\Parser\PhpParser;

class AnnotationComponent extends AbstractComponent
{
    private Collection $annotations;

    public function __construct(protected PhpParser $parser)
    {
        parent::__construct($this->parser);
        $this->annotations = new Collection();
    }

    public function isEmpty(): bool
    {
        return $this->annotations->isEmpty();
    }

    public function isNotEmpty(): bool
    {
        return $this->annotations->isNotEmpty();
    }

    public function add(AbstractAnnotation $annotation): bool
    {
        if ($this->has($annotation->getType(), $annotation->getName())) {
            return false;
        }

        $this->annotations->add($annotation);

        return true;
    }

    public function has(string $type, string $name): bool
    {
        return $this->annotations
            ->filter(fn(AbstractAnnotation $annotation) => $annotation->getType() === $type && $annotation->getName() === $name)
            ->isNotEmpty();
    }

    private function filter(string|array $types): Collection
    {
        if (is_string($types)) {
            $types = [$types];
        }

        return $this->annotations
            ->filter(fn(AbstractAnnotation $annotation) => in_array($annotation->getType(), $types));
    }

    private function reject(string|array $types): Collection
    {
        if (is_string($types)) {
            $types = [$types];
        }

        return $this->annotations
            ->reject(fn(AbstractAnnotation $annotation) => in_array($annotation->getType(), $types));
    }

    public function toString(): string|null
    {
        if ($this->annotations->isNotEmpty()) {
            return implode(PHP_EOL, array_merge(
                ['/**'],
                $this->filter('property')->map(fn(PropertyAnnotation $annotation) => $annotation->toString())->toArray(),
                $this->filter('method')->map(fn(MethodAnnotation $annotation) => $annotation->toString())->toArray(),
                $this->reject(['property', 'method'])->map(fn(AbstractAnnotation $annotation) => $annotation->toString())->toArray(),
                [' */'],
            ));
        }

        return null;
    }
}
