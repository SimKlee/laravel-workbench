<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components;

use PhpParser\Modifiers;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\PropertyItem;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\UnionType;
use PhpParser\Node\VarLikeIdentifier;
use SimKlee\LaravelWorkbench\PhpParser\PhpParserHelper;

class PropertyComponent extends AbstractComponent
{
    public const MODIFIER_PUBLIC    = Modifiers::PUBLIC;
    public const MODIFIER_PROTECTED = Modifiers::PROTECTED;
    public const MODIFIER_PRIVATE   = Modifiers::PRIVATE;
    public const MODIFIER_READONLY  = Modifiers::READONLY;
    public const MODIFIER_STATIC    = Modifiers::STATIC;

    public function has(string $name): bool
    {
        return $this->parser->findInstanceOf(Property::class, $this->class)
            ->filter(fn(Property $property) => current($property->props)->name->name === $name)
            ->isNotEmpty();
    }

    public function add(string            $name,
                        array|string|null $types = null,
                        int|null          $modifier = self::MODIFIER_PUBLIC,
                        mixed             $default = null,
                        bool              $presetNull = false): bool
    {
        if ($this->has($name)) {
            return false;
        }

        if (!is_null($default)) {
            $default = PhpParserHelper::valueToObject($default);
        } elseif ($presetNull) {
            $default = new ConstFetch(name: new Name('null'));
        }

        $this->class->stmts[] = new Property(
            flags: $modifier,
            props: [new PropertyItem(
                        name   : new VarLikeIdentifier($name),
                        default: $default,
                    )],
            type : $this->getType($types)
        );

        return true;
    }

    public function get(string $name): Property|null
    {
        return $this->parser->findProperty($name);
    }

    public function remove(string $name): bool
    {
        if ($this->has($name)) {
            $this->class->stmts = collect($this->class->stmts)
                ->reject(fn(Stmt $stmt) => $stmt instanceof Property && current($stmt->props)->name->name === $name)
                ->toArray();

            return true;
        }

        return false;
    }

    protected function getType(array|string|null $types): UnionType|Identifier|Name|null
    {
        if (is_null($types)) {
            return null;
        }

        if (is_array($types)) {
            return new UnionType(
                collect($types)
                    ->map(function ($type) {
                        return $this->getType($type);
                    })->toArray()
            );
        }

        return $this->getType($types);
    }

    public function addArrayItem(string $name, ArrayItem $item): bool
    {
        if ($this->has($name) === false) {
            return false;
        }

        $property     = $this->get($name);
        $propertyItem = current($property->props);

        if (is_null($propertyItem->default)) {
            $propertyItem->default = new Array_([$item]);

            return true;
        }

        if ($propertyItem->default instanceof Array_) {
            $propertyItem->default->items[] = $name;

            return true;
        }

        return false;
    }
}
