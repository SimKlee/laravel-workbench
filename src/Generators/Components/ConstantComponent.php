<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components;

use PhpParser\Modifiers;
use PhpParser\Node\Identifier;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Const_;

class ConstantComponent extends AbstractComponent
{
    public function has(string $name): bool
    {
        return $this->parser->findInstanceOf(ClassConst::class, $this->class)
            ->filter(fn(ClassConst $const) => current($const->consts)->name->name === $name)
            ->isNotEmpty();
    }

    public function add(string $name, mixed $value, int $modifier = Modifiers::PUBLIC): bool
    {
        if ($this->has($name)) {
            return false;
        }

        $this->class->stmts[] = new ClassConst(
            consts: [
                new Const_(
                    name : new Identifier($name),
                    value: $this->parser->valueToObject($value)
                ),
            ],
            flags : $modifier
        );

        return true;
    }

    public function remove(string $name): bool
    {
        if ($this->has($name) === false) {
            return false;
        }

        $this->class->stmts[] = collect($this->class->stmts)
            ->reject(fn(Stmt $stmt) => $stmt instanceof ClassConst && current($stmt->consts)->name->name === $name)
            ->toArray();

        return true;
    }

    public function get(string $name): ClassConst|null
    {
        return collect($this->class->stmts)
            ->filter(fn(Stmt $stmt) => $stmt instanceof ClassConst && current($stmt->consts)->name->name === $name)
            ->first();
    }

    public function setValue(string $name, mixed $value): bool
    {
        if ($this->has($name) === false) {
            return false;
        }

        current($this->get($name)->consts)->value = $this->parser->valueToObject($value);

        return true;
    }

    public function changeOrCreate(string $name, mixed $value, int $modifier = Modifiers::PUBLIC): bool
    {
        return $this->setValue($name, $value) || $this->add($name, $value, $modifier);
    }
}
