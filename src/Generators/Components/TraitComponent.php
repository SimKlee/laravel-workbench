<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components;

use PhpParser\Node\Name;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\TraitUse;
use SimKlee\LaravelWorkbench\Parser\PhpParser;
use SimKlee\LaravelWorkbench\Tools\FQN;

class TraitComponent extends AbstractComponent
{
    public function __construct(protected PhpParser           $parser,
                                private readonly UseComponent $useComponent,
                                protected string              $base = self::BASE_CLASS)
    {
        parent::__construct($this->parser, $this->base);
    }

    public function add(string $trait): bool
    {
        if ($this->has($trait)) {
            return false;
        }

        $this->useComponent->add($trait);
        $this->base()->stmts[] = new TraitUse(traits: [new Name(FQN::className($trait))]);

        return true;
    }

    public function has(string $trait): bool
    {
        return $this->parser->findInstanceOf(TraitUse::class, $this->base())
            ->filter(fn(TraitUse $traitUse) => current($traitUse->traits)->name === class_basename($trait))
            ->isNotEmpty();
    }

    public function remove(string $trait): bool
    {
        if ($this->has($trait)) {
            $this->namespace->stmts = collect($this->namespace->stmts)
                ->reject(fn(Stmt $stmt) => $stmt instanceof TraitUse && current($stmt->traits)->name->name === $trait)
                ->toArray();

            return true;
        }

        return false;
    }
}
