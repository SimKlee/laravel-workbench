<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components;

use Illuminate\Support\Collection;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Use_;
use PhpParser\Node\UseItem;
use SimKlee\LaravelWorkbench\Ast\UseBuilder;
use SimKlee\LaravelWorkbench\Tools\FQN;

class UseComponent extends AbstractComponent
{
    public function all(): Collection
    {
        return $this->parser->findInstanceOf(Use_::class, $this->namespace);
    }

    public function add(string $class, string $alias = null): bool
    {
        if (FQN::namespaceEqual(FQN::namespace($class), $this->namespace->name->name)) {
            return false;
        }

        if ($this->has($class)) {
            return false;
        }

        $this->namespace->stmts[] = UseBuilder::create($class, $alias)->object();

        return true;
    }

    public function has(string $class): bool
    {
        return $this->parser->findInstanceOf(Use_::class, $this->namespace)
            ->filter(fn(Use_ $use) => current($use->uses)->name->name === $class)
            ->isNotEmpty();
    }

    public function remove(string $class): bool
    {
        if ($this->has($class)) {
            $this->namespace->stmts = collect($this->namespace->stmts)
                ->reject(fn(Stmt $stmt) => $stmt instanceof Use_ && current($stmt->uses)->name->name === $class)
                ->toArray();

            return true;
        }

        return false;
    }
}
