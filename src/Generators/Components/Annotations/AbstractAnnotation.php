<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components\Annotations;

abstract class AbstractAnnotation
{
    abstract public function getName(): string;

    abstract public function getType(): string;

    abstract public function toString(): string;

    public static function getDefault(mixed $default): mixed
    {
        return match (true) {
            is_array($default)  => '[]',
            is_null($default)   => 'null',
            is_string($default) => sprintf("'%s'", $default),
            is_bool($default)   => $default ? 'true' : 'false',
            default             => $default,
        };
    }
}
