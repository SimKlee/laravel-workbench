<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components\Annotations;

use SimKlee\LaravelWorkbench\String\StringBuffer;

class MethodAnnotationParam
{
    private bool  $hasDefault = false;
    private mixed $default = null;
    private array $types = [];

    public function __construct(private readonly string $name)
    {
    }

    public static function create(string $name): self
    {
        return new self($name);
    }

    public function type(string $type): self
    {
        $this->types[] = $type;

        return $this;
    }

    public function default($value): self
    {
        $this->hasDefault = true;
        $this->default    = $value;

        return $this;
    }

    public function toString(): string
    {
        return StringBuffer::create(implode('|', $this->types))
            ->append(' $' . $this->name)
            ->appendIf($this->hasDefault, ' = ' . AbstractAnnotation::getDefault($this->default))
            ->toString();
    }
}
