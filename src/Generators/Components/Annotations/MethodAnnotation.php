<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators\Components\Annotations;

use SimKlee\LaravelWorkbench\String\StringBuffer;

class MethodAnnotation extends AbstractAnnotation
{
    protected bool  $static = false;
    protected array $types  = [];
    protected array $params = [];

    public function __construct(private readonly string $name)
    {
    }

    public static function create(string $name): self
    {
        return new self($name);
    }

    public function static(): self
    {
        $this->static = true;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return 'method';
    }

    public function type(string $type): self
    {
        $this->types[] = $type;

        return $this;
    }

    public function param(MethodAnnotationParam $param): self
    {
        $this->params[] = $param->toString();

        return $this;
    }

    public function toString(): string
    {
        return StringBuffer::create(' * @method')
            ->appendIf($this->static, ' static')
            ->appendIf(count($this->types) > 0, ' ' . implode('|', $this->types))
            ->append(' ' . $this->name . '(' . implode(', ', $this->params) . ')')
            ->toString();
    }
}
