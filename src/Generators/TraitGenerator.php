<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Exception;
use Illuminate\Support\Facades\File;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelWorkbench\Generators\Components\AbstractComponent;
use SimKlee\LaravelWorkbench\Generators\Components\MethodComponent;
use SimKlee\LaravelWorkbench\Generators\Components\TraitComponent;
use SimKlee\LaravelWorkbench\Generators\Components\UseComponent;
use SimKlee\LaravelWorkbench\Parser\PhpParser;
use SimKlee\LaravelWorkbench\Tools\FQN;

class TraitGenerator
{
    protected PhpParser    $parser;
    public UseComponent    $useComponent;
    public TraitComponent  $traitComponent;
    public MethodComponent $methodComponent;

    public function __construct(protected string $trait, string $templateFile = null)
    {
        $file = $this->getFile();
        if (File::exists($file) === false && is_null($templateFile)) {
            throw new Exception(sprintf('Trait %s does not exists. You must set a template file!', $this->trait));
        }

        $file                  = File::exists($file) ? $file : $templateFile;
        $this->parser          = new PhpParser($file);
        $this->useComponent    = new UseComponent($this->parser);
        $this->traitComponent  = new TraitComponent($this->parser, $this->useComponent, AbstractComponent::BASE_TRAIT);
        $this->methodComponent = new MethodComponent($this->parser, AbstractComponent::BASE_TRAIT);

        $this->init();
    }

    protected function init(): void
    {
        if (!empty($this->trait)) {
            $this->setTraitName(FQN::className($this->trait));
            $this->setNamespace(FQN::namespace($this->trait));
        }
    }

    public function setTraitName(string $name): self
    {
        $this->parser->trait->name = new Identifier(name: $name);

        return $this;
    }

    public function setNamespace(string $namespace): self
    {
        $this->parser->namespace->name = new Name(name: $namespace);

        return $this;
    }

    public function getFile(): string
    {
        return FQN::file($this->trait);
    }

    public function write(string $file = null, bool $overwrite = true): bool
    {
        if (is_null($file)) {
            $file = $this->getFile();
        }

        if ($overwrite === false && File::exists($file)) {
            return false;
        }

        $this->parser->sort();

        $parts = explode('/', $file);
        array_pop($parts);
        $path = implode('/', $parts);
        if (File::isDirectory($path) === false) {
            File::makeDirectory(path: $path, recursive: true);
        }

        return File::put($file, (new Standard())->prettyPrintFile($this->parser->ast)) !== false;
    }
}
