<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use PhpParser\BuilderFactory;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;

class ModelQueryGenerator extends ClassGenerator
{
    protected ModelDefinition $modelDefinition;
    protected array           $annotations = [];
    protected BuilderFactory  $builder;

    public function __construct(protected string $file, protected readonly ModelDefinitionCollection $modelDefinitions, protected readonly string $modelName)
    {
        parent::__construct($file);
        $this->modelDefinition = $this->modelDefinitions->get($this->modelName);
        $this->builder         = new BuilderFactory();
        $this->process();
    }

    /**
     * @throws \Exception
     */
    public static function fromModelTemplate(ModelDefinitionCollection $modelDefinitions, string $modelName, string $file): ModelQueryGenerator
    {
        return new ModelQueryGenerator($file, $modelDefinitions, $modelName);
    }

    /**
     * @throws \Exception
     */
    public static function fromModel(ModelDefinitionCollection $modelDefinitions, Model $model): ModelQueryGenerator
    {
        $parts     = explode('\\', $model::class);
        $modelName = array_pop($parts);
        $file      = app_path(sprintf('Models/Query/%sQuery.php', $modelName));

        return new ModelQueryGenerator($file, $modelDefinitions, $modelName);
    }

    public function generate(?string $file = null): int|false
    {
        if (is_null($file)) {
            $file = base_path(sprintf('%s/%sQuery.php', $this->namespaceToPath(config('laravel-workbench.query.namespace')), $this->modelDefinition->model()));
        }

        $this->parser->sort();

        return $this->parser->write($file ?? $this->file);
    }

    protected function process(): void
    {
        $this->setClassName($this->modelDefinition->model() . 'Query');
        $this->setNamespace(config('laravel-workbench.query.namespace'));
        $this->addUses([
            sprintf('%s\%s', config('laravel-workbench.query.namespace'), $this->modelDefinition->model()),
            Collection::class,
        ]);
    }
}
