<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use PhpParser\BuilderFactory;
use PhpParser\Modifiers;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\Match_;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\MatchArm;
use PhpParser\Node\Name;
use PhpParser\Node\NullableType;
use PhpParser\Node\Param;
use PhpParser\Node\Scalar\Float_;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Return_;
use PhpParser\Node\UnionType;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\PhpObjects\ArrayItem;
use SimKlee\LaravelWorkbench\PhpObjects\Constant;
use SimKlee\LaravelWorkbench\PhpParser\PhpParserHelper;

class ModelMetaGenerator extends ClassGenerator
{
    protected ModelDefinition $modelDefinition;
    protected array $annotations = [];
    protected BuilderFactory $builder;

    public function __construct(protected string $file,
                                protected readonly ModelDefinitionCollection $modelDefinitions,
                                protected readonly string $modelName)
    {
        parent::__construct($file, __DIR__ . '/../Templates/ModelMetaTemplate.php');

        $this->modelDefinition = $this->modelDefinitions->get($this->modelName);
        $this->process();
        $this->builder = new BuilderFactory();
    }

    public static function fromModelTemplate(ModelDefinitionCollection $modelDefinitions, string $modelName, string $file): ModelMetaGenerator
    {
        return new ModelMetaGenerator($file, $modelDefinitions, $modelName);
    }

    public static function fromModel(ModelDefinitionCollection $modelDefinitions, Model $model): ModelMetaGenerator
    {
        $parts     = explode('\\', $model::class);
        $modelName = array_pop($parts);
        $file      = app_path(sprintf('Models/Meta/%sMeta.php', $modelName));

        return new ModelMetaGenerator($file, $modelDefinitions, $modelName);
    }

    public function generate(?string $file = null): int|false
    {
        if (is_null($file)) {
            $file = base_path(sprintf('%s/%sMetaNew.php', $this->namespaceToPath(config('laravel-workbench.meta.namespace')), $this->modelDefinition->model()));
        }

        $this->sort();

        return File::put($file, (new Standard())->prettyPrintFile($this->ast));
    }

    protected function process(): void
    {
        $this->setClassName($this->modelDefinition->model() . 'Meta');
        $this->setNamespace(config('laravel-workbench.meta.namespace'));

        $this->useComponent->add(sprintf('%s\%s', config('laravel-workbench.meta.namespace'), $this->modelDefinition->model()));
        $this->useComponent->add(Collection::class);

        $this->processMethods();
    }

    protected function processMethods(): void
    {
        $this->addMatchAssocMethod('max', $this->modelDefinition->columns->withLength(), '?int', 'length');
        $this->addMatchAssocMethod('min', $this->modelDefinition->columns->withLength(), '?int', null, 1);
        $this->addMatchAssocMethod('default', $this->modelDefinition->columns->defaults(), 'mixed', 'default');
        $this->addMatchAssocMethod('decimals', $this->modelDefinition->columns->withDecimals(), '?int', 'decimals');
        $this->addReturnCollectionMethod('unsigned', $this->modelDefinition->columns->unsigned());
        $this->addReturnCollectionMethod('requirements', $this->modelDefinition->columns->required());
        $this->addReturnCollectionMethod('nullables', $this->modelDefinition->columns->nullables());
    }

    protected function addMatchAssocMethod(string $name, Collection $properties, string $returnType, ?string $columnMethod, mixed $fixedValue = null, bool $withDefault = true): void
    {
        $this->addClassStmt(
            $this->createMatchAssocMethod(
                name       : $name,
                array      : $properties
                    ->map(
                        fn (ColumnDefinition $column) => new ArrayItem(
                            value: ! is_null($columnMethod) ? call_user_func([$column, $columnMethod]) : $fixedValue,
                            key  : sprintf('PROPERTY_%s', Str::upper($column->name()))
                        )
                    )
                    ->toArray(),
                returnType : $returnType,
                withDefault: $withDefault
            )
        );
    }

    protected function addReturnCollectionMethod(string $name, Collection $properties): void
    {
        $this->addClassStmt(
            $this->createReturnCollectionMethod(
                name : $name,
                array: $properties->map(
                    fn (ColumnDefinition $column) => new ArrayItem(
                        value: new Constant([
                            $this->modelDefinition->model(),
                            sprintf('PROPERTY_%s', Str::upper($column->name())),
                        ])
                    )
                )->toArray()
            )
        );
    }

    protected function createMatchAssocMethod(string $name, array $array, string $returnType = 'void', bool $withDefault = false): ClassMethod
    {
        $method           = new ClassMethod(new Identifier($name));
        $method->params[] = new Param(var: new Variable('property'), type: new Identifier('string'));
        $method->flags    = Modifiers::PUBLIC;

        if (Str::startsWith($returnType, '?')) {
            $returnType = Str::substr($returnType, 1);
            if (Str::contains($returnType, '\\')) {
                $this->addUse($returnType);
                $parts      = explode('\\', $returnType);
                $returnType = array_pop($parts);
            }
            $method->returnType = new NullableType(new Identifier($returnType));
        } elseif (Str::contains($returnType, '|')) {
            $method->returnType = new UnionType(
                collect(explode('|', $returnType))
                    ->map(fn (string $type) => new Identifier($type))
                    ->toArray()
            );
        } else {
            $method->returnType = new Identifier($returnType);
        }

        $arms = collect($array)->map(fn (ArrayItem $item) => new MatchArm(
            conds: [
                new ClassConstFetch(
                    class: new Name($this->modelDefinition->model()),
                    name : new Identifier($item->key)
                ),
            ],
            body : match (gettype($item->value)) {
                'integer' => new Int_($item->value),
                'string'  => new String_($item->value),
                'double'  => new Float_($item->value),
                'boolean' => new ConstFetch(new Name($item->value ? 'true' : 'false')),
                'NULL'    => new ConstFetch(new Name('null')),
            }
        ))->toArray();

        if ($withDefault) {
            $arms[] = new MatchArm(conds: null, body: new ConstFetch(new Name('null')));
        }

        $method->stmts = [new Return_(expr: new Match_(cond: new Variable('property'), arms: $arms))];

        return $method;
    }

    protected function createReturnCollectionMethod(string $name, array $array): ClassMethod
    {
        $method             = new ClassMethod(new Identifier($name));
        $method->flags      = PhpParserHelper::getFlags('public');
        $method->returnType = new Name('Collection');

        $method->stmts = [
            new Return_(
                expr: new FuncCall(
                    name: new Name('collect'),
                    args: [
                        new Arg(
                            value: new Array_(
                                items: collect($array)
                                    ->map(fn (ArrayItem $item) => $item->toArrayItem())
                                    ->toArray()
                            )
                        ),
                    ]
                )
            ),
        ];

        return $method;
    }
}
