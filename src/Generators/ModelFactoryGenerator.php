<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use PhpParser\BuilderFactory;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt\Return_;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\String\StringBuffer;

class ModelFactoryGenerator extends ClassGenerator
{
    protected ModelDefinition $modelDefinition;
    protected array           $annotations = [];
    protected BuilderFactory  $builder;

    public function __construct(protected string                             $file,
                                protected readonly ModelDefinitionCollection $modelDefinitions,
                                protected readonly string                    $modelName)
    {
        parent::__construct($file, __DIR__ . '/../Templates/ModelFactoryTemplate.php');

        $this->modelDefinition = $this->modelDefinitions->get($this->modelName);
        $this->process();
        $this->builder = new BuilderFactory();
    }

    /**
     * @throws \Exception
     */
    public static function fromModel(ModelDefinitionCollection $modelDefinitions, Model $model): self
    {
        $parts     = explode('\\', $model::class);
        $modelName = array_pop($parts);
        $file      = app_path(sprintf('Models/Meta/%sMeta.php', $modelName));

        return new ModelFactoryGenerator($file, $modelDefinitions, $modelName);
    }

    public function generate(?string $file = null): int|false
    {
        if (is_null($file)) {
            $file = base_path(sprintf('database/factories/%sFactory.php', $this->modelDefinition->model()));
        }

        $this->parser->sort();

        return $this->write($file);
    }

    protected function process(): void
    {
        $this->setClassName($this->modelDefinition->model() . 'Factory');
        $this->setExtends(config('laravel-workbench.factory.extends'));
        $this->setNamespace(config('laravel-workbench.factory.namespace'));
        $this->addUses([
            sprintf('App\Models\%s', $this->modelDefinition->model()),
            Model::class,
        ]);
        $this->setClassComment(
            StringBuffer::create()
                ->append('/**')
                ->append(sprintf(' * @extends Factory<%s>', $this->modelDefinition->model()))
                ->append(sprintf(' * @method %s create(array $attributes = [], ?Model $parent = null)', $this->modelDefinition->model()))
                ->append(' */')
                ->toString()
        );

        $this->processDefinitionMethod();
    }

    private function processDefinitionMethod(): void
    {
        $this->parser->findClassMethod('definition')->stmts = [
            new Return_(expr: new Array_(
                items: $this->modelDefinition->columns->all()
                    ->map(fn(ColumnDefinition $columnDefinition) => new ArrayItem(
                        value: new ConstFetch(name: new Name(name: 'null')),
                        key  : new ClassConstFetch(
                            class: new Name(name: $columnDefinition->model()),
                            name : new Identifier(name: 'PROPERTY_' . Str::upper($columnDefinition->name()))
                        )
                    ))->toArray()
            )),
        ];
    }
}
