<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use PhpParser\Comment\Doc;
use PhpParser\Modifiers;
use PhpParser\Node;
use PhpParser\Node\Const_;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\PropertyItem;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\TraitUse;
use PhpParser\Node\Stmt\Use_;
use PhpParser\Node\UnionType;
use PhpParser\Node\UseItem;
use PhpParser\Node\VarLikeIdentifier;
use PhpParser\NodeAbstract;
use PhpParser\NodeFinder;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelWorkbench\PhpParser\PhpParserHelper;

class ClassGeneratorOld
{
    protected array $ast = [];
    protected NodeFinder $finder;
    protected Namespace_ $namespace;
    protected Class_ $class;

    public function __construct(protected string $file)
    {
        $this->ast = (new ParserFactory())->createForNewestSupportedVersion()
            ->parse(File::get($this->file));

        $this->finder    = new NodeFinder();
        $this->namespace = $this->finder->findFirstInstanceOf($this->ast, Namespace_::class);
        $this->class     = $this->finder->findFirstInstanceOf($this->ast, Class_::class);
    }

    public function generate(string $file): int|false
    {
        return File::put($file, (new Standard())->prettyPrintFile($this->ast));
    }

    public function setClassName(string $className): void
    {
        $this->class->name = new Identifier($className);
    }

    public function setNamespace(string $namespace): void
    {
        $this->namespace->name = new Name($namespace);
    }

    public function setExtends(string $class): void
    {
        if (Str::contains($class, '\\')) {
            $this->addUse($class);
            $parts = explode('\\', $class);
            $class = array_pop($parts);
        }

        $this->class->extends = new Name($class);
    }

    public function addInterface(string $class): void
    {
        if (Str::contains($class, '\\')) {
            $this->addUse($class);
            $parts = explode('\\', $class);
            $class = array_pop($parts);
        }

        $this->class->implements[] = new Name($class);
    }

    public function hasUse(string $class): bool
    {
        return collect($this->finder->findInstanceOf($this->namespace, Use_::class))
                ->filter(fn (Use_ $use) => current($use->uses)->name->name === $class)
                ->count() > 0;
    }

    public function addUse(string $class, ?string $alias = null): bool
    {
        if ($this->inSameNamespace($class)) {
            return false;
        }

        if ($this->hasUse($class)) {
            return false;
        }

        $this->namespace->stmts[] = new Use_([new UseItem(new Name($class), $alias)]);

        return true;
    }

    public function removeUse(string $class): void
    {
        $this->class->stmts = collect($this->class->stmts)
            ->reject(fn (Stmt $stmt) => $stmt instanceof Use_ && current($stmt->uses)->name->name === $class)
            ->toArray();
    }

    public function hasProperty(string $name): bool
    {
        return collect($this->finder->findInstanceOf($this->ast, Property::class))
                ->filter(fn (Property $property) => current($property->props)->name->name === $name)
                ->count() > 0;
    }

    public function removeProperty(string $name): void
    {
        $this->class->stmts = collect($this->class->stmts)
            ->reject(fn (Stmt $stmt) => $stmt instanceof Property && current($stmt->props)->name->name === $name)
            ->toArray();
    }

    public function addProperty(string $name, string $visibility, array|string|null $types = null, mixed $default = null, bool $readonly = false, bool $static = false, ?string $comment = null): bool
    {
        if ($this->hasProperty($name)) {
            return false;
        }

        if ($static && $readonly) {
            Log::error(sprintf('A static property con not be readonly! [%s]', $name));
            $readonly = false;
        }

        $flags = PhpParserHelper::getFlags($visibility, $readonly, $static);

        if (! is_null($default)) {
            $default = $this->valueToObject($default);
        }

        if ($readonly && ! is_null($default)) {
            Log::error(sprintf('Readonly property %s can not have a default value! Set default to NULL.', $name));
            $default = null;
        }

        $this->class->stmts[] = new Property(
            flags: $flags,
            props: [new PropertyItem(name: new VarLikeIdentifier($name), default: $default)],
            type : $this->getTypes($types)
        );

        return true;
    }

    public function changeProperty(string $name, string $visibility, array|string|null $types = null, mixed $default = null, bool $readonly = false, bool $static = false): bool
    {
        if ($this->hasProperty($name) === false) {
            return false;
        }

        /** @var Property $property */
        $property = collect($this->class->stmts)
            ->filter(fn (Stmt $stmt) => $stmt instanceof Property && current($stmt->props)->name->name === $name)
            ->first();

        if ($static && $readonly) {
            Log::error(sprintf('A static property con not be readonly! [%s]', $name));
            $readonly = false;
        }

        $property->flags = PhpParserHelper::getFlags($visibility, $readonly, $static);

        if (! is_null($default)) {
            $default = $this->valueToObject($default);
        }

        if ($readonly && ! is_null($default)) {
            Log::error(sprintf('Readonly property %s can not have a default value! Set default to NULL.', $name));
            $default = null;
        }

        $property->props = [new PropertyItem(name: new VarLikeIdentifier($name), default: $default)];
        $property->type  = $this->getTypes($types);

        return true;
    }

    public function hasConstant(string $name): bool
    {
        return collect($this->finder->findInstanceOf($this->ast, ClassConst::class))
                ->filter(fn (ClassConst $const) => current($const->consts)->name->name === $name)
                ->count() > 0;
    }

    public function addConstant(string $name, mixed $value = null, string $visibility = 'public'): bool
    {
        if ($this->hasConstant($name)) {
            return false;
        }

        $this->class->stmts[] = new ClassConst(
            consts: [new Const_(name: new Identifier($name), value: $this->valueToObject($value))],
            flags : PhpParserHelper::getFlags($visibility)
        );

        return true;
    }

    public function removeConstant(string $name): void
    {
        $this->class->stmts = collect($this->class->stmts)
            ->reject(fn (Stmt $stmt) => $stmt instanceof ClassConst && current($stmt->consts)->name->name === $name)
            ->toArray();
    }

    public function changeConstant(
        string $name,
        mixed $value = null,
        string $visibility = 'public'
    ): bool {
        if ($this->hasConstant($name) === false) {
            return false;
        }

        /** @var ClassConst $constant */
        $constant = collect($this->class->stmts)
            ->filter(fn (Stmt $stmt) => $stmt instanceof ClassConst && current($stmt->consts)->name->name === $name)
            ->first();

        $constant->flags                  = PhpParserHelper::getFlags($visibility);
        current($constant->consts)->value = PhpParserHelper::valueToObject($value);

        return true;
    }

    public function hasTrait(string $name): bool
    {
        return collect($this->finder->findInstanceOf($this->ast, TraitUse::class))
                ->filter(fn (TraitUse $traitUse) => current($traitUse->traits)->name === $name)
                ->count() > 0;
    }

    public function addTrait(string $name): bool
    {
        if ($this->hasTrait($name)) {
            return false;
        }

        $this->addUse($name);
        $trait                = last(explode('\\', $name));
        $this->class->stmts[] = new TraitUse(traits: [new Name($trait)]);

        return true;
    }

    public function removeTrait(string $name, bool $withUse = true): void
    {
        $className          = last(explode('\\', $name));
        $this->class->stmts = collect($this->class->stmts)
            ->reject(fn (Stmt $stmt) => $stmt instanceof TraitUse && (current($stmt->traits)->name === $name || current($stmt->traits)->name === $className))
            ->toArray();

        if ($withUse) {
            $this->removeUse($name);
        }
    }

    public function sort(): void
    {
        $this->namespace->stmts = array_merge(
            collect($this->namespace->stmts)->filter(fn (NodeAbstract $stmt) => $stmt instanceof Use_)->toArray(),
            collect($this->namespace->stmts)->filter(fn (NodeAbstract $stmt) => $stmt instanceof Class_)->toArray(),
        );

        $this->class->stmts = array_merge(
            collect($this->class->stmts)->filter(fn (Stmt $stmt) => $stmt instanceof TraitUse)->toArray(),
            collect($this->class->stmts)->filter(fn (Stmt $stmt) => $stmt instanceof ClassConst)->toArray(),
            collect($this->class->stmts)->filter(fn (Stmt $stmt) => $stmt instanceof Property)->toArray(),
            collect($this->class->stmts)->filter(fn (Stmt $stmt) => $stmt instanceof ClassMethod)->toArray(),
        );
    }

    public function traverse(array $visitors): void
    {
        $traverser = new NodeTraverser();

        collect($visitors)
            ->each(fn (NodeVisitorAbstract $visitor) => $traverser->addVisitor($visitor));

        $this->ast = $traverser->traverse($this->ast);
    }

    public function setClassComment(string $comment): void
    {
        $this->class->setAttribute('comments', [new Doc($comment)]);
    }

    public function addClassStmt(Node $stmt): void
    {
        $this->class->stmts[] = $stmt;
    }

    public function addNamespaceStmt(Node $stmt): void
    {
        $this->namespace->stmts[] = $stmt;
    }

    public function getAst(): array
    {
        return $this->ast;
    }

    protected function getTypes(array|string|null $types): UnionType|Identifier|Name|null
    {
        if (is_null($types)) {
            return null;
        }

        if (is_array($types)) {
            return new UnionType(
                collect($types)
                    ->map(function ($type) {
                        return $this->getType($type);
                    })->toArray()
            );
        }

        return $this->getType($types);
    }

    protected function getType(string $type): Identifier|Name|null
    {
        if (Str::contains($type, '\\')) {
            $this->addUse($type);
            $parts = explode('\\', $type);

            return new Name(last($parts));
        }

        return new Identifier($type);
    }

    protected function valueToObject($value): mixed
    {
        return PhpParserHelper::valueToObject($value);
    }

    protected function namespaceToPath(string $namespace): string
    {
        return Str::ucfirst(Str::replace('\\', '/', $namespace));
    }

    protected function pathToNamespace(string $path): string
    {
        return collect(explode('/', $path))
            ->map(fn (string $item) => Str::ucfirst($item))
            ->implode('\\');
    }

    private function inSameNamespace(string $class): bool
    {
        $classParts = explode('\\', $class);
        array_pop($classParts);
        $classNamespace = implode('\\', $classParts);

        return $classNamespace === $this->namespace->name->name;
    }

    public static function getClassNameFromFQN(string $fqn): string
    {
        return last(explode('\\', $fqn));
    }

    public static function getNamespaceNameFromFQN(string $fqn): string
    {
        $parts = explode('\\', $fqn);
        array_pop($parts);

        return implode('\\', $parts);
    }

    public static function getFileFromFQN(string $fqn): string
    {
        return Str::replace(['\\', 'App'], ['/', 'app'], $fqn) . '.php';
    }

    public function hasMethod(string $name): bool
    {
        return collect($this->finder->findInstanceOf($this->ast, ClassMethod::class))
            ->filter(fn(ClassMethod $classMethod) => $classMethod->name->name === $name)
            ->isNotEmpty();
    }
}
