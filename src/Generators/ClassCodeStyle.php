<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Illuminate\Support\Facades\File;
use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelWorkbench\NodeVisitors\SortClassStmts;

class ClassCodeStyle
{
    public array $ast = [];
    public ?string $file;
    private NodeTraverser $traverser;

    public function __construct()
    {
        $this->traverser = new NodeTraverser();
    }

    public static function fromFile(string $file): ClassCodeStyle
    {
        $instance       = new self();
        $instance->file = $file;
        $parser         = (new ParserFactory())->createForNewestSupportedVersion();
        $instance->ast  = $parser->parse(File::get($file));

        return $instance;
    }

    public static function fromAST(array $stmts): ClassCodeStyle
    {
        $instance      = new self();
        $instance->ast = $stmts;

        return $instance;
    }

    public function write(?string $file = null): bool|int
    {
        if (is_null($file)) {
            $file = $this->file;
        }

        if (is_null($file)) {
            throw new \Exception('Missing file name to write file!');
        }

        $this->traverser->addVisitor(new SortClassStmts());
        $this->ast = $this->traverser->traverse($this->ast);

        return File::put($file, (new Standard())->prettyPrintFile($this->ast));
    }
}
