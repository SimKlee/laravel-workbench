<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Exception;
use Illuminate\Support\Facades\File;
use PhpParser\Comment\Doc;
use PhpParser\Node\DeclareItem;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Declare_;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\TraitUse;
use PhpParser\Node\Stmt\Use_;
use PhpParser\NodeAbstract;
use PhpParser\PrettyPrinter\Standard;
use SimKlee\LaravelWorkbench\Generators\Components\AnnotationComponent;
use SimKlee\LaravelWorkbench\Generators\Components\ConstantComponent;
use SimKlee\LaravelWorkbench\Generators\Components\MethodComponent;
use SimKlee\LaravelWorkbench\Generators\Components\PropertyComponent;
use SimKlee\LaravelWorkbench\Generators\Components\TraitComponent;
use SimKlee\LaravelWorkbench\Generators\Components\UseComponent;
use SimKlee\LaravelWorkbench\Parser\PhpParser;
use SimKlee\LaravelWorkbench\Tools\FQN;

class EnumerationGenerator
{
    protected PhpParser $parser;

    public function __construct(protected string $class, string $templateFile = null)
    {
        if (File::exists(FQN::file($this->class)) === false && is_null($templateFile)) {
            throw new Exception(sprintf('Class %s does not exists. You must set a template class!', $class));
        }

        $file         = File::exists(FQN::file($this->class)) ? FQN::file($this->class) : $templateFile;
        $this->parser = new PhpParser(FQN::file($file));

        $this->init();
    }

    protected function init(): void
    {
        $this->setNamespace(FQN::namespace($this->class));
    }

    public function setNamespace(string $namespace): self
    {
        $this->parser->namespace->name = new Name(name: $namespace);

        return $this;
    }
}
