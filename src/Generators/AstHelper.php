<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Generators;

use Illuminate\Support\Str;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;

class AstHelper
{
    public static function propertyConst(ColumnDefinition $columnDefinition, bool $self = false): ClassConstFetch
    {
        return new ClassConstFetch(
            class: new Name(name: $self ? 'self' : $columnDefinition->model()),
            name : new Identifier(name: sprintf('PROPERTY_%s', Str::upper($columnDefinition->name())))
        );
    }
}
