<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Tools;

use Illuminate\Support\Str;

class FQN
{
    public static function isFQN(string $class): bool
    {
        return Str::contains($class, '\\');
    }

    public static function className(string $fqn): string
    {
        return class_basename($fqn);
    }

    public static function namespace(string $fqn): string
    {
        $parts = explode('\\', $fqn);
        array_pop($parts);
        return implode('\\', $parts);
    }

    public static function file(string $fqn): string
    {
        $parts = explode('\\', $fqn);
        $file = collect($parts)
            ->map(function (string $part, int $i) use ($parts) {
                if ($i === 0 && $part === 'App') {
                    return 'app';
                }
                if ($i === 0 && $part === 'Tests') {
                    return 'tests';
                }
                if ($i === 0 && $part === 'Database') {
                    return 'database';
                }
                if ($i === 1 && $part === 'Factories' && $parts[0] === 'Database') {
                    return 'factories';
                }

                return $part;
            })->implode('/');

        if (Str::endsWith($file, '.php') === false) {
            $file .= '.php';
        }

        return $file;
    }

    public static function namespaceEqual(string $namespace1, string $namespace2): bool
    {
        return $namespace1 === $namespace2;
    }

    public static function namespaceToPath(string $namespace): string
    {
        if ($namespace === 'Database\Factories') {
            return 'database\factories';
        }

        return collect(explode('\\', $namespace))
            ->map(function (string $part) {
                return match ($part) {
                    'App'   => 'app',
                    'Tests' => 'tests',
                    default => $part,
                };
            })->implode('/');
    }
}
