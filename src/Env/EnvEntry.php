<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Env;

class EnvEntry implements EntryItemInterface
{
    public function __construct(public string $name, public ?string $value = null)
    {

    }

    public static function fromString(string $line): self
    {
        $parts = explode('=', $line);

        return new EnvEntry(array_shift($parts), implode('=', $parts));
    }
}
