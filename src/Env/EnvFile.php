<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Env;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class EnvFile
{
    /** @var Collection|EntryItemInterface[] */
    private Collection $items;

    public function __construct(private ?string $file = null)
    {
        if (is_null($this->file)) {
            $this->file = base_path('.env');
        }

        $this->items = File::lines($this->file)
            ->collect()
            ->map(fn(string $line) => match (true) {
                empty($line)                => new EnvBlank(),
                Str::startsWith($line, '#') => new EnvComment($line),
                default                     => EnvEntry::fromString($line),
            });
    }

    public function keys(): array
    {
        return $this->items
            ->filter(fn(EntryItemInterface $entry) => $entry instanceof EnvEntry)
            ->map(fn(EnvEntry $entry) => $entry->name)
            ->toArray();
    }

    public function has(string $name): bool
    {
        return $this->items
            ->filter(fn(EntryItemInterface $entry) => $entry instanceof EnvEntry)
            ->filter(fn(EnvEntry $entry) => $entry->name === $name)
            ->isNotEmpty();
    }

    public function hasNot(string $name): bool
    {
        return $this->has($name) === false;
    }

    public function getEnvEntry(string $name): EnvEntry|null
    {
        return $this->items
            ->filter(fn(EntryItemInterface $entry) => $entry instanceof EnvEntry)
            ->filter(fn(EnvEntry $entry) => $entry->name === $name)
            ->first();
    }

    public function setEnvEntry(string $name, $value): void
    {
        if (is_bool($value)) {
            $value = ($value === true) ? 'true' : 'false';
        }

        if ($this->hasNot($name)) {
            $this->items->add(new EnvEntry(name: $name, value: $value));
            return;
        }

        $this->getEnvEntry($name)->value = (string)$value;
    }

    public function write(string $file = null)
    {
        if (is_null($file)) {
            $file = $this->file;
        }

        File::put($file, $this->items
            ->map(fn(EntryItemInterface $entry) => match (true) {
                $entry instanceof EnvBlank   => '',
                $entry instanceof EnvComment => $entry->value,
                $entry instanceof EnvEntry   => sprintf('%s=%s', $entry->name, $entry->value),
            })->implode(PHP_EOL)
        );
    }

    public function getFile(bool $basename = false): ?string
    {
        if ($basename) {
            return basename($this->file);
        }

        return $this->file;
    }
}
