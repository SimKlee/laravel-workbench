<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Env;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;

class ProjectPortMap
{
    private array $projectPorts = [];

    public function __construct(private readonly string $projectPath, private readonly array $ignoreEnvVars = [])
    {
        $this->scan();
    }

    /**
     * @throws DirectoryNotFoundException
     */
    public function scan(): void
    {
        collect(File::directories($this->projectPath))
            ->filter(fn(string $projectPath) => File::exists(sprintf('%s/.env', $projectPath)))
            ->each(function (string $projectPath) {
                $project = last(explode('/', $projectPath));
                $env     = new EnvFile(sprintf('%s/.env', $projectPath));

                $this->projectPorts[$project] = collect($env->keys())
                    ->filter(fn(string $key) => Str::contains($key, 'PORT') && !in_array($key, $this->ignoreEnvVars))
                    ->filter(fn(string $key) => (int) $env->getEnvEntry($key)->value > 0)
                    ->mapWithKeys(fn(string $key) => [$env->getEnvEntry($key)->name => (int) $env->getEnvEntry($key)->value]);
            });
    }

    public function getProjectPorts(): array
    {
        return $this->projectPorts;
    }

    public function getUsedPorts(): array
    {
        return collect($this->projectPorts)
            ->map(fn(Collection $ports, string $project) => $ports)
            ->flatten()
            ->sort()
            ->values()
            ->toArray();
    }

    public function used(int $port): string|false
    {
        foreach ($this->projectPorts as $project => $ports) {
            foreach ($ports as $key => $value) {
                if ($value === $port) {
                    return sprintf('%s -> %s', $project, $key);
                }
            }
        }

        return false;
    }

    public function getUnusedPort(int $min, int $max): int|false
    {
        foreach (range($min, $max) as $port) {
            if ($this->used($port) === false) {
                return $port;
            }
        }

        return false;
    }
}
