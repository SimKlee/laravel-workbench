<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Env;

use Illuminate\Support\Str;

class EnvComment implements EntryItemInterface
{
    public function __construct(public string $value)
    {
        if (Str::startsWith($this->value, '#') === false) {
            $this->value = '#' . $this->value;
        }
    }
}
