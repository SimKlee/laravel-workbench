<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Blade\Nodes;

abstract class AbstractNode
{
    public array $subNodes = [];

    abstract public function toString(): string;
}
