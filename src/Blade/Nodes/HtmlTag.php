<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Blade\Nodes;

use Illuminate\Support\Collection;

class HtmlTag extends AbstractNode
{
    public ?string    $name;
    public Collection $attributes;

    public function __construct()
    {
        $this->attributes = new Collection();
    }

    public function setAttribute(TagAttribute $tagAttribute): void
    {
        $this->attributes->add($tagAttribute);
    }

    public function toString(): string
    {
        $string = sprintf('<%s%s', $this->name, $this->attributes->isNotEmpty() ? ' ' : '');
        $string .= $this->attributes->map(fn(TagAttribute $tagAttribute) => $tagAttribute->toString())->implode(' ');
        $string .= '>' . PHP_EOL;
        $string .= collect($this->subNodes)->map(fn(AbstractNode $node) => $node->toString())->implode(PHP_EOL);
        $string .= sprintf('</%s>', $this->name);

        return $string;
    }
}
