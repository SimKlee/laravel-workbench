<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Blade\Nodes;

use Illuminate\Support\Collection;

class TagAttribute extends AbstractNode
{
    public ?string $name;
    public mixed   $value;

    public function toString(): string
    {
        return sprintf('%s="%s"', $this->name, $this->value);
    }
}
