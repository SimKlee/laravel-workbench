<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Templates;

use App\View\InputType;
use Illuminate\Support\Collection;

class ModelMetaTemplate
{
    public function inputType(string $property): ?InputType
    {
        return match ($property) {
            // Model::PROPERTY_X => InputType::INPUT_STRING,
            default => InputType::INPUT_STRING,
        };
    }

    public function values(string $property): ?Collection
    {
        return match ($property) {
            'foo'   => 'bar',
            default => null,
        };
    }
}
