<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Templates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

abstract class AbstractModelMeta
{
    protected Model $model;

    public function __construct(Model|string $model)
    {
        $this->model = is_string($model) ? new $model() : $model;
    }

    public function guarded(): Collection
    {
        return collect($this->model->getGuarded());
    }

    public function isGuarded(string $property): bool
    {
        return $this->model->isGuarded($property);
    }

    public function fillable(): Collection
    {
        return collect($this->model->getFillable());
    }

    public function isFillable(string $property): bool
    {
        return $this->fillable()->contains($property);
    }

    public function dates(): Collection
    {
        return collect($this->model->getDates());
    }

    public function isDate(string $property): bool
    {
        return $this->dates()->contains($property);
    }

    public function isRequired(string $property): ?bool
    {
        return $this->requirements()->contains($property);
    }

    public function isNullable(string $property): ?bool
    {
        return $this->nullables()->contains($property);
    }

    public function isUnsigned(string $property): ?bool
    {
        return $this->unsigned($property)->contains($property);
    }

    abstract public function requirements(): Collection;

    abstract public function nullables(): Collection;

    abstract public function unsigned(string $property): Collection;

    abstract public function inputType(string $property): string;

    abstract public function values(string $property): ?Collection;

    abstract public function min(string $property): ?int;

    abstract public function max(string $property): ?int;

    abstract public function decimals(string $property): ?int;

    abstract public function default(string $property): mixed;
}
