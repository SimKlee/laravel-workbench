<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Templates;

use Illuminate\Database\Eloquent\Factories\Factory;

class ModelFactoryTemplate extends Factory
{
    public function definition(): array
    {
        return [];
    }
}
