<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Templates;

use Illuminate\Database\Eloquent\Model;

class ModelTemplate extends Model
{
    public const TABLE = '';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = self::TABLE;

    /**
     * @var array|string[]
     */
    protected $fillable = [];

    /**
     * @var array|string[]
     */
    protected $guarded = [];

    /**
     * @var array|string[]
     */
    protected $hidden = [];

    /**
     * @var array
     */
    protected $attributes = [];
}
