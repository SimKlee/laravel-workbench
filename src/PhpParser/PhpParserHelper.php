<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\PhpParser;

use Illuminate\Support\Str;
use PhpParser\Modifiers;
use PhpParser\Node\ArrayItem;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\Float_;
use PhpParser\Node\Scalar\Int_;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\UnionType;
use SimKlee\LaravelWorkbench\PhpObjects\ArrayItem as CustomArrayItem;

final class PhpParserHelper
{
    public static function valueToObject($value): mixed
    {
        return match (gettype($value)) {
            'integer' => new Int_($value),
            'string'  => new String_($value),
            'double'  => new Float_($value),
            'boolean' => new ConstFetch(new Name($value ? 'true' : 'false')),
            'NULL'    => new ConstFetch(new Name('null')),
            'array'   => self::arrayToObject($value),
            'object'  => match (get_class($value)) {
                CustomArrayItem::class => $value->toArrayItem(),
                default                => $value,
            },
        };
    }

    public static function arrayToObject(array $array): ?Array_
    {
        return new Array_(
            items: collect($array)
                ->map(fn ($value, $key) => self::createArrayItem($value, $key))
                ->toArray()
        );
    }

    public static function createArrayItem($value, $key): ?ArrayItem
    {
        $value = self::valueToObject($value);
        if ($value instanceof ArrayItem) {
            return $value;
        }
        $key = self::valueToObject($key);

        return new ArrayItem($value, $key);
    }

    public static function getFlags(
        string $visibility,
        bool $readonly = false,
        bool $static = false
    ): int {
        $flags = match ($visibility) {
            'private'   => Modifiers::PRIVATE,
            'protected' => Modifiers::PROTECTED,
            'public'    => Modifiers::PUBLIC,
        };

        if ($readonly) {
            $flags += Modifiers::READONLY;
        }

        if ($static) {
            $flags += Modifiers::STATIC;
        }

        return $flags;
    }

    public static function getTypes(array|string|null $types): UnionType|Identifier|Name|null
    {
        if (is_null($types)) {
            return null;
        }

        if (is_array($types)) {
            return new UnionType(collect($types)->map(fn ($type) => self::getType($type))->toArray());
        }

        return self::getType($types);
    }

    public static function getType(string $type): Identifier|Name|null
    {
        if (Str::contains($type, '\\')) {
            return new Name($type);
        }

        return new Identifier($type);
    }
}
