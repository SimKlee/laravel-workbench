<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema;

use Illuminate\Support\Collection;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\ForeignKeyDTO;

class ForeignKeys extends AbstractSchema
{
    /**
     * @return Collection|ForeignKeyDTO[]
     */
    public function get(string $table, string $database = null): Collection
    {
        return $this->db
            ->table('information_schema.TABLE_CONSTRAINTS AS TC')
            ->leftJoin('information_schema.KEY_COLUMN_USAGE AS KCU', 'TC.CONSTRAINT_NAME', 'KCU.CONSTRAINT_NAME')
            ->leftJoin('information_schema.REFERENTIAL_CONSTRAINTS AS RC', 'TC.CONSTRAINT_NAME', 'RC.CONSTRAINT_NAME')
            ->select([
                'TC.*',
                'KCU.COLUMN_NAME',
                'KCU.REFERENCED_TABLE_NAME',
                'KCU.REFERENCED_COLUMN_NAME',
                'RC.UPDATE_RULE',
                'RC.DELETE_RULE',
            ])
            ->where('TC.TABLE_SCHEMA', $database ?? $this->database)
            ->where('TC.TABLE_NAME', $table)
            ->where('TC.CONSTRAINT_TYPE', 'FOREIGN KEY')
            ->get()
            ->map(fn($row) => new ForeignKeyDTO($row));
    }
}
