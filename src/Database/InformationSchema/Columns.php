<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema;

use Illuminate\Support\Collection;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\ColumnDTO;

class Columns extends AbstractSchema
{
    /**
     * @return Collection|ColumnDTO[]
     */
    public function get(string $table, string $database = null): Collection
    {
        return $this->db
            ->table('information_schema.COLUMNS')
            ->where('TABLE_SCHEMA', $database ?? $this->database)
            ->where('TABLE_NAME', $table)
            ->orderBy('ORDINAL_POSITION')
            ->get()
            ->map(fn($row) => new ColumnDTO($row));
    }
}
