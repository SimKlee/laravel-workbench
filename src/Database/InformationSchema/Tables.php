<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema;

use Illuminate\Support\Collection;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\TableDTO;

class Tables extends AbstractSchema
{
    /**
     * @return Collection|TableDTO[]
     */
    public function get(string $database = null): Collection
    {
        return $this->db
            ->table('information_schema.TABLES')
            ->where('TABLE_SCHEMA', $database ?? $this->database)
            ->get()
            ->map(fn($row) => new TableDTO($row));
    }
}
