<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema;

use Illuminate\Support\Collection;
use SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects\IndexDTO;

class Indexes extends AbstractSchema
{
    /**
     * @return Collection|IndexDTO[]
     */
    public function get(string $table, string $database = null): Collection
    {
        return $this->db
            ->table('information_schema.STATISTICS')
            ->select([
                'TABLE_SCHEMA',
                'TABLE_NAME',
                'NON_UNIQUE',
                'INDEX_NAME',
                'CARDINALITY',
                'NULLABLE',
            ])
            ->selectRaw('GROUP_CONCAT(COLUMN_NAME ORDER BY SEQ_IN_INDEX) AS COLUMNS')
            ->where('TABLE_SCHEMA', $database ?? $this->database)
            ->where('TABLE_NAME', $table)
            ->groupBy('TABLE_SCHEMA')
            ->groupBy('TABLE_NAME')
            ->groupBy('INDEX_NAME')
            ->get()
            ->map(fn($row) => new IndexDTO($row));
    }
}
