<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema;

use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;

abstract class AbstractSchema
{
    protected Connection $db;
    protected string     $database;

    public function __construct(protected ?string $connection = null)
    {
        if (is_null($this->connection)) {
            $this->connection = config('database.default');
        }

        $this->database = config(sprintf('database.connections.%s.database', $this->connection));

        $this->db = DB::connection($this->connection)
            ->setDatabaseName('information_schema');
    }

    public static function create(string $connection = null): static
    {
        return new static($connection);
    }
}
