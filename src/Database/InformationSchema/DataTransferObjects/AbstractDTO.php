<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionProperty;
use stdClass;

abstract class AbstractDTO
{
    public function __construct(stdClass|array $data = [])
    {
        $this->fillData($data);
    }

    protected function fillData(stdClass|array $data): void
    {
        if ($data instanceof stdClass) {
            $data = json_decode(json_encode($data), true);
        }

        collect((new ReflectionClass($this))->getProperties(ReflectionProperty::IS_PUBLIC))
            ->each(function (ReflectionProperty $property) use ($data) {
                $key = Str::upper(Str::snake($property->name));
                if (isset($data[$key])) {
                    try {
                        $this->{$property->name} = $this->castValue($property->getType()->getName(), $data[$key]);
                    } catch (Exception $e) {
                        Log::error($e->getMessage());
                    }
                }
            });
    }

    protected function filterBool($value): bool
    {
        if ($value === 'YES' || $value === 'Y') {
            return true;
        }

        if ($value === 'NO' || $value === 'N' || $value === 'NEVER') {
            return false;
        }

        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @throws Exception
     */
    protected function castValue(string $type, $value): mixed
    {
        return match ($type) {
            'string'        => (string) $value,
            'int'           => filter_var($value, FILTER_VALIDATE_INT),
            'float'         => filter_var($value, FILTER_VALIDATE_FLOAT),
            'bool'          => $this->filterBool($value),
            'Carbon\Carbon' => Carbon::parse($value),
            default         => throw new Exception('Unknown type ' . $type),
        };
    }

    public function toArray(): array
    {
        return collect((new ReflectionClass($this))->getProperties(ReflectionProperty::IS_PUBLIC))
            ->mapWithKeys(fn(ReflectionProperty $property) => [$property->name => $this->{$property->name}])
            ->toArray();
    }

    public function toJson(int $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE): string
    {
        return json_encode($this->toArray(), $flags);
    }
}
