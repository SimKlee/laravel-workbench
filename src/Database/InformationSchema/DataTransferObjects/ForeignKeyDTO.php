<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects;

class ForeignKeyDTO extends AbstractDTO
{
    public ?string $constraintSchema     = null;
    public ?string $constraintName       = null;
    public ?string $tableSchema          = null;
    public ?string $tableName            = null;
    public ?string $columnName           = null;
    public ?string $constraintType       = null;
    public ?string $referencedTableName  = null;
    public ?string $referencedColumnName = null;
    public ?string $updateRule           = null;
    public ?string $deleteRule           = null;
}
