<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects;

use Exception;
use Illuminate\Support\Str;
use stdClass;

class ColumnDTO extends AbstractDTO
{
    public ?string $tableCatalog            = null;
    public ?string $tableSchema             = null;
    public ?string $tableName               = null;
    public ?string $columnName              = null;
    public ?int    $ordinalPosition         = null;
    public ?string $columnDefault           = null;
    public ?bool   $isNullable              = null;
    public ?bool   $isUnsigned              = null;
    public ?string $dataType                = null;
    public ?int    $characterMaximumLength  = null;
    public ?int    $characterOctetLength    = null;
    public ?int    $numericPrecision        = null;
    public ?int    $numericScale            = null;
    public ?int    $datetimePrecision       = null;
    public ?string $characterSetName        = null;
    public ?string $collationName           = null;
    public ?string $columnType              = null;
    public ?string $columnKey               = null;
    public ?string $extra                   = null;
    public ?string $privileges              = null;
    public ?string $columnComment           = null;
    public ?bool   $isGenerated             = null;
    public ?int    $generationExpression    = null;
    public ?bool   $isSystemTimePeriodStart = null;
    public ?bool   $isSystemTimePeriodEnd   = null;

    protected function fillData(stdClass|array $data): void
    {
        parent::fillData($data);
        $this->isUnsigned = Str::endsWith($this->columnType, 'unsigned');
    }

    public function phpType(): string
    {
        return match ($this->dataType) {
            'int'                                   => 'int',
            'char', 'varchar'                       => 'string',
            'timestamp', 'date', 'datetime', 'time' => 'Carbon',
            default                                 => throw new Exception($this->dataType),
        };
    }
}
