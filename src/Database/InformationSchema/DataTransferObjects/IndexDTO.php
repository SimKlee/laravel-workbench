<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects;

class IndexDTO extends AbstractDTO
{
    public ?string $tableSchema = null;
    public ?string $tableName   = null;
    public ?bool   $nonUnique   = null;
    public ?string $indexName   = null;
    public ?int    $cardinality = null;
    public ?bool   $nullable    = null;
    public ?string $columns     = null;
}
