<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Database\InformationSchema\DataTransferObjects;

use Carbon\Carbon;

class TableDTO extends AbstractDTO
{
    public ?string $tableCatalog   = null;
    public ?string $tableSchema    = null;
    public ?string $tableName      = null;
    public ?string $tableType      = null;
    public ?string $engine         = null;
    public ?int    $version        = null;
    public ?string $rawFormat      = null;
    public ?int    $tableRows      = null;
    public ?int    $avgRowLength   = null;
    public ?int    $dataLength     = null;
    public ?int    $maxDataLength  = null;
    public ?int    $indexLength    = null;
    public ?int    $dataFree       = null;
    public ?int    $autoIncrement  = null;
    public ?Carbon $createTime     = null;
    public ?Carbon $updateTime     = null;
    public ?Carbon $checkTime      = null;
    public ?string $tableCollation = null;
    public ?int    $checksum       = null;
    public ?string $createOptions  = null;
    public ?string $tableComment   = null;
    public ?int    $maxIndexLength = null;
    public ?bool   $temporary      = null;
}
