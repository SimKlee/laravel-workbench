<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Console\Commands;

use Illuminate\Console\Command;

abstract class AbstractCommand extends Command
{
    protected $signature   = '';
    protected $description = '';

    abstract public function handle(): void;

    protected function clearScreen(): void
    {
        $this->line("\033c");
    }
}
