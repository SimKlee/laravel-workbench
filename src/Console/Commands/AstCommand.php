<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;
use SimKlee\LaravelWorkbench\NodeVisitors\RemoveAttributes;

final class AstCommand extends Command
{
    protected $signature   = 'ast {file}
                                  {--method=}
                                  {--json}
                                  {--s|short}
                                  {--o|output}';
    protected $description = 'Get the AST of a PHP file.';
    private array $ast     = [];

    public function handle(): void
    {
        $parser    = (new ParserFactory())->createForHostVersion();
        $this->ast = $parser->parse(File::get($this->argument('file')));

        if ($this->option('short')) {
            $traverser = new NodeTraverser();
            $traverser->addVisitor(new RemoveAttributes());
            $this->ast = $traverser->traverse($this->ast);
        }

        if ($this->option('method')) {
            dump($this->getMethod($this->option('method')));

            return;
        }

        if ($this->option('json')) {
            if ($this->option('output')) {
                File::put(base_path('ast.json'), json_encode($this->ast, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
                return;
            }

            dump(json_encode($this->ast, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
        }

        dump($this->ast);
    }

    private function getNamespace(): ?Namespace_
    {
        return collect($this->ast)
            ->filter(fn ($item) => $item instanceof Namespace_)
            ->first();
    }

    private function getClass(): ?Class_
    {
        $namespace = $this->getNamespace();
        if ($namespace) {
            return collect($namespace->stmts)
                ->filter(fn ($item) => $item instanceof Class_)
                ->first();
        }

        return null;
    }

    private function getMethod(string $name): ?ClassMethod
    {
        $class = $this->getClass();
        if ($class) {
            return collect($class->stmts)
                ->filter(fn ($item) => $item instanceof ClassMethod)
                ->filter(fn (ClassMethod $method) => $method->name->name === $name)
                ->first();
        }

        return null;
    }
}
