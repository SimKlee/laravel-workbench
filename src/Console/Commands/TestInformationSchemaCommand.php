<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\NodeTraverser;
use PhpParser\ParserFactory;
use SimKlee\LaravelWorkbench\Database\InformationSchema\Columns;
use SimKlee\LaravelWorkbench\Database\InformationSchema\ForeignKeys;
use SimKlee\LaravelWorkbench\Database\InformationSchema\Tables;
use SimKlee\LaravelWorkbench\NodeVisitors\RemoveAttributes;

final class TestInformationSchemaCommand extends Command
{
    protected $signature   = 'test:is';
    protected $description = '';

    public function handle(): void
    {
        $table = new ForeignKeys();

        dump($table->get('telescope_entries_tags'));
    }
}
