<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Console\Commands;

use Illuminate\Console\Command;
use SimKlee\LaravelPrototype\Definitions\Collections\ModelDefinitionCollection;
use SimKlee\LaravelWorkbench\Generators\ModelGenerator;
use SimKlee\LaravelWorkbench\Generators\ModelMetaGenerator;

class ReadClassCommand extends Command
{
    protected $signature   = 'read:model {model}';
    protected $description = '';

    public function handle(): void
    {
        $modelDefinitions = ModelDefinitionCollection::load();
        /*
        $generator        = ModelGenerator::fromModelTemplate(
            $modelDefinitions,
            $this->argument('model'),
            base_path('packages/laravel-workbench/src/Templates/ModelTemplate.php')
        );
        $generator->generate(app_path(sprintf('Models2/%s.php', $this->argument('model'))));
        */

        $generator = ModelMetaGenerator::fromModelTemplate(
            $modelDefinitions,
            $this->argument('model'),
            base_path('packages/laravel-workbench/src/Templates/ModelMetaTemplate.php')
        );
        $generator->generate(app_path(sprintf('Models2/%sMeta.php', $this->argument('model'))));

        dump($generator->getAst());
    }
}
