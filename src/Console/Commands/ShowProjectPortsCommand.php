<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Console\Commands;

use Illuminate\Console\Command;
use SimKlee\LaravelWorkbench\Env\ProjectPortMap;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;

use function Laravel\Prompts\error;
use function Laravel\Prompts\intro;
use function Laravel\Prompts\info;
use function Laravel\Prompts\warning;

final class ShowProjectPortsCommand extends Command
{
    protected $signature   = 'project:ports {project-directory?} {--test=} {--s|short}';
    protected $description = 'Shows used ports inside project folder for Laravel projects';

    private ProjectPortMap $map;

    public function handle(): void
    {
        intro($this->description);

        try {
            $this->map = new ProjectPortMap(
                projectPath  : $this->argument('project-directory'),
                ignoreEnvVars: [
                    'APP_PORT',
                    'DB_PORT',
                    'REDIS_PORT',
                    'MAIL_PORT',
                    'PUSHER_PORT',
                    'MAILTRAP_MAIL_PORT',
                    'MAILHOG_MAIL_PORT',
                    'PRODUCTDB_PORT',
                    'PLANTUML_PORT',
                ]
            );

            if ($this->option('test')) {
                $this->testPort();
                return;
            }

            $this->showPorts();

        } catch (DirectoryNotFoundException $e) {
            error($e->getMessage());
            warning('Dont\'t send this command through sail. Inside the docker stack there is no access to directories outside!');
        }
    }

    private function testPort(): void
    {
        $used = $this->map->used((int) $this->option('test'));

        if ($used === false) {
            info(sprintf('Port %s is not in used by your projects. Feel free to use it.', $this->option('test')));
            return;
        }

        warning(sprintf('Port %s is in use by %s', $this->option('test'), $used));
    }

    private function showPorts(): void
    {
        if ($this->option('short')) {
            $this->info('Used ports');
            $this->components->bulletList($this->map->getUsedPorts());
            return;
        }

        collect($this->map->getProjectPorts())
            ->each(function (mixed $ports, string $project) {
                $this->info(sprintf('PROJECT: %s', $project));
                $this->table(['VARIABLE', 'PORT'], collect($ports)
                    ->map(fn(int $port, string $key) => [$key, $port]));
            });
    }
}
