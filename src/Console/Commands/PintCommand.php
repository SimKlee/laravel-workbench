<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Process;

class PintCommand extends Command
{
    protected $signature   = 'pint {file}';
    protected $description = '';

    public function handle(): void
    {
        $command = sprintf('%s %s', base_path('vendor/bin/pint'), $this->argument('file'));
        Process::run($command);
    }
}
