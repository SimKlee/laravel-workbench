<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Console\Commands;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SimKlee\LaravelWorkbench\Env\EnvFile;

use function Laravel\Prompts\select;
use function Laravel\Prompts\table;

class SyncEnvFilesCommand extends AbstractCommand
{
    protected $signature   = 'workbench:env:sync {--list} {--only-diff}';
    protected $description = 'Sync ENV files.';

    private EnvFile $env1;
    private EnvFile $env2;

    private int   $maxLength  = 55;
    private array $ignoreKeys = [
        'equals'     => [
            'APP_KEY',
            'APP_ENV',
        ],
        'startsWith' => [],
        'endsWith'   => [
            'API_KEY',
            'TOKEN',
        ],
        'contains'   => [],
    ];

    public function handle(): void
    {
        $this->env1 = new EnvFile(base_path('.env'));
        $this->env2 = new EnvFile(base_path('.env.example'));

        if ($this->option('list')) {
            $this->listValues();
            return;
        }

        $this->sync();
    }

    private function getKeys(): Collection
    {
        return collect($this->env1->keys())
            ->merge($this->env2->keys())
            ->unique()
            ->reject(fn(string $key) => $this->ignoreKey($key));
    }

    private function ignoreKey(string $key): bool
    {
        $equals = collect($this->ignoreKeys['equals'])
            ->filter(fn(string $pattern) => $pattern === $key)
            ->isNotEmpty();

        if ($equals) {
            return true;
        }

        $startsWith = collect($this->ignoreKeys['startsWith'])
            ->filter(fn(string $pattern) => Str::startsWith($key, $pattern))
            ->isNotEmpty();

        if ($startsWith) {
            return true;
        }

        $endsWith = collect($this->ignoreKeys['endsWith'])
            ->filter(fn(string $pattern) => Str::endsWith($key, $pattern))
            ->isNotEmpty();

        if ($endsWith) {
            return true;
        }

        $contains = collect($this->ignoreKeys['contains'])
            ->filter(fn(string $pattern) => Str::contains($key, $pattern))
            ->isNotEmpty();

        if ($contains) {
            return true;
        }

        return false;
    }

    public function listValues(): void
    {
        $rows = $this->getKeys()
            ->map(fn(string $key) => [
                $key,
                implode(PHP_EOL, str_split($this->env1->getEnvEntry($key)?->value ?? '', $this->maxLength)),
                implode(PHP_EOL, str_split($this->env2->getEnvEntry($key)?->value ?? '', $this->maxLength)),
            ]);

        if ($this->option('only-diff')) {
            $rows = $rows->reject(fn(array $row) => $row[1] == $row[2]);
        }

        table(
            headers: ['Key', $this->env1->getFile(basename: true), $this->env2->getFile(basename: true)],
            rows   : $rows
        );
    }

    private function sync(): void
    {
        $this->getKeys()
            ->map(fn(string $key) => [
                $key,
                $this->env1->getEnvEntry($key)?->value ?? '',
                $this->env2->getEnvEntry($key)?->value ?? '',
            ])->reject(fn(array $row) => $row[1] == $row[2])
            ->each(function (array $row) {
                if ($this->env1->hasNot($row[0])) {
                    $this->env1->setEnvEntry($row[0], $row[2]);
                } elseif ($this->env2->hasNot($row[0])) {
                    $this->env2->setEnvEntry($row[0], $row[1]);
                } elseif ($row[1] !== $row[2]) {
                    $env = select(
                        label  : $row[0],
                        options: [
                            'env1' => sprintf('%s (%s)', $row[1], $this->env1->getFile(basename: true)),
                            'env2' => sprintf('%s (%s)', $row[2], $this->env2->getFile(basename: true)),
                            'skip' => 'skip',
                        ],
                        default: 'skip'
                    );
                    match ($env) {
                        'env1'  => $this->env2->setEnvEntry($row[0], $row[1]),
                        'env2'  => $this->env1->setEnvEntry($row[0], $row[2]),
                        default => null,
                    };
                }
            });

        $this->env1->write();
        $this->env2->write();
    }

}
