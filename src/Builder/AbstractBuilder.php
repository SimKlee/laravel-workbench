<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Builder;

use PhpParser\BuilderFactory;

abstract class AbstractBuilder
{
    protected BuilderFactory $factory;

    public function __construct()
    {
        $this->factory = new BuilderFactory();
    }

    protected function propertyMultilineDocComment(string $type): string
    {
        return implode(PHP_EOL, [
            '/**',
            ' * @var ' . $type,
            ' */',
        ]);
    }
}
