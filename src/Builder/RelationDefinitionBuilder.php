<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Builder;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use PhpParser\Node;
use PhpParser\Node\Arg;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Stmt\Return_;
use SimKlee\LaravelPrototype\Definitions\RelationDefinition;

class RelationDefinitionBuilder extends AbstractBuilder
{
    public function __construct(private readonly RelationDefinition $relation)
    {
        parent::__construct();
    }

    /**
     * @return Collection|string[]
     */
    public function getUses(): Collection
    {
        return collect([
            Collection::class,
            sprintf('Illuminate\Database\Eloquent\Relations\%s', $this->relation->type()),
        ]);
    }

    public function getConstants(): Collection
    {
        return collect([
            sprintf('WITH_%s', $this->getModelName(['snake', 'upper'])) => $this->getModelName(['camel', 'lcFirst']),
        ]);
    }

    public function getModelRelationMethodAnnotation(): string
    {
        return sprintf(
            ' * @property Collection|%s%s $%s',
            $this->relation->relatedModel(),
            $this->relation->type() === 'HasMany' ? '[]' : '',
            $this->getModelName(['camel', 'lcFirst'])
        );
    }

    public function getModelRelationMethod(): Node
    {
        $methodName = Str::lcfirst(Str::camel($this->relation->relatedModel()));
        if ($this->relation->type() === 'HasMany') {
            $methodName .= 's';
        }

        return $this->factory
            ->method($methodName)
            ->makePublic()
            ->setReturnType($this->relation->type())
            ->addStmt(
                new Return_(
                    new MethodCall(
                        var : new Variable('this'),
                        name: new Identifier(Str::lcfirst($this->relation->type())),
                        args: [
                            new Arg(
                                new ClassConstFetch(
                                    class: new Name($this->relation->relatedModel()),
                                    name : new Identifier('class')
                                )
                            ),
                        ]
                    )
                )
            )->getNode();
    }

    private function getModelName(array $modifiers = []): string
    {
        $modelName = $this->relation->relatedModel();
        if ($this->relation->type()) {
            $modelName .= 's';
        }

        foreach ($modifiers as $modifier) {
            $modelName = call_user_func([Str::class, $modifier], $modelName);
        }

        return $modelName;
    }
}
