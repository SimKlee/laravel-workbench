<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Builder;

use Illuminate\Support\Str;
use PhpParser\Node;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Stmt\Return_;
use SimKlee\LaravelPrototype\Definitions\ColumnDefinition;
use SimKlee\LaravelPrototype\Definitions\ModelDefinition;
use SimKlee\LaravelWorkbench\PhpObjects\ArrayItem;
use SimKlee\LaravelWorkbench\PhpObjects\Constant;

class ModelDefinitionBuilder extends AbstractBuilder
{
    public function __construct(private readonly ModelDefinition $definition)
    {
        parent::__construct();
    }

    public function getCastsProperty(): Node
    {
        return $this->factory->property('casts')
            ->makeProtected()
            ->setDocComment($this->propertyMultilineDocComment('array'))
            ->setDefault(
                new Array_(items: $this->definition->columns->all()
                    ->reject(fn (ColumnDefinition $column) => $column->cast() === 'string')
                    ->map(fn (ColumnDefinition $column) => (new ArrayItem(
                        value: $this->getCast($column),
                        key  : new Constant([$this->definition->model(), sprintf('PROPERTY_%s', Str::upper($column->name()))])
                    ))->toArrayItem())
                    ->toArray())
            )->getNode();
    }

    public function getCastsMethod(): Node
    {
        return $this->factory
            ->method('casts')
            ->makeProtected()
            ->setReturnType('array')
            ->addStmt(new Return_(
                expr: new Array_(
                    items: $this->definition->columns->all()
                        ->reject(fn (ColumnDefinition $column) => $column->cast() === 'string')
                        ->map(fn (ColumnDefinition $column) => (new ArrayItem(
                            value: $this->getCast($column),
                            key  : new Constant([$this->definition->model(), sprintf('PROPERTY_%s', Str::upper($column->name()))])
                        ))->toArrayItem())
                        ->toArray()
                )
            ))->getNode();
    }

    private function getCast(ColumnDefinition $column): Constant|string
    {
        $cast = $column->cast();

        if ($cast === 'Carbon') {
            //return new Constant(['Carbon', 'class']);
            return 'date';
        }

        if (Str::endsWith($cast, 'Enum')) {
            return new Constant([$cast, 'class']);
        }

        return $cast;
    }
}
