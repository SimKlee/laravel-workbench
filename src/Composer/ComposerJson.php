<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Composer;

use Illuminate\Support\Facades\File;
use SimKlee\LaravelWorkbench\Composer\DataTransferObjects\ComposerDto;

final class ComposerJson
{
    private array $data;

    public ComposerDto $composerDto;

    public function __construct(private readonly ?string $file = null)
    {
        if ($this->file) {
            $this->data        = json_decode(json: File::get($this->file), associative: true);
            $this->composerDto = new ComposerDto($this->data);
        }
    }

    public function write(?string $file = null, bool $overwrite = true): bool
    {
        if (is_null($file)) {
            $file = $this->file;
        }

        if ($overwrite || File::exists($file) === false) {
            return File::put($file, json_encode(
                    $this->composerDto->toArray(),
                    JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
                )) > 0;
        }

        return false;
    }

}
