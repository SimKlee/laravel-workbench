<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Composer\DataTransferObjects;

/**
 * @see https://getcomposer.org/doc/articles/scripts.md#event-names
 */
final class ScriptDto extends AbstractDto
{
    public const PRE_INSTALL_CMD         = 'pre-install-cmd';
    public const POST_INSTALL_CMD        = 'post-install-cmd';
    public const PRE_UPDATE_CMD          = 'pre-update-cmd';
    public const POST_UPDATE_CMD         = 'post-update-cmd';
    public const PRE_STATUS_CMD          = 'pre-status-cmd';
    public const POST_STATUS_CMD         = 'post-status-cmd';
    public const PRE_ARCHIVE_CMD         = 'pre-archive-cmd';
    public const POST_ARCHIVE_CMD        = 'post-archive-cmd';
    public const PRE_AUTOLOAD_CMD        = 'pre-autoload-cmd';
    public const POST_AUTOLOAD_CMD       = 'post-autoload-cmd';
    public const POST_ROOT_PACKAGE_CMD   = 'post-root-package-install';
    public const POST_CREATE_PROJECT_CMD = 'post-create-project-cmd';
    public const PRE_PACKAGE_INSTALL     = 'pre-package-install';
    public const POST_PACKAGE_INSTALL    = 'post-package-install';
    public const PRE_PACKAGE_UPDATE     = 'pre-package-update';
    public const POST_PACKAGE_UPDATE    = 'post-package-update';
    public const PRE_PACKAGE_UNINSTALL     = 'pre-package-uninstall';
    public const POST_PACKAGE_UNINSTALL    = 'post-package-uninstall';


    public ?string $name    = null;
    public array   $scripts = [];

    public function hasScript(string $script): bool
    {
        return in_array($script, $this->scripts);
    }

    public function addScript(string $script): void
    {
        if ($this->hasScript($script)) {
            return;
        }

        $this->scripts[] = $script;
    }

    protected function fromArray(array $data): void
    {
        $this->name    = $data['name'] ?? null;
        $this->scripts = $data['scripts'] ?? [];
    }

    public function toArray(): array
    {
        return [$this->name => $this->scripts];
    }
}
