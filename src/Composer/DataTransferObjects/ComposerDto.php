<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Composer\DataTransferObjects;

use Illuminate\Support\Collection;

final class ComposerDto extends AbstractDto
{
    public const TYPE_PROJECT         = 'project';
    public const TYPE_LIBRARY         = 'library';
    public const TYPE_METAPACKAGE     = 'metapackage';
    public const TYPE_COMPOSER_PLUGIN = 'composer-plugin';

    public const TYPE_DEV    = 'dev';
    public const TYPE_ALPHA  = 'alpha';
    public const TYPE_BETA   = 'beta';
    public const TYPE_RC     = 'RC';
    public const TYPE_STABLE = 'stable';

    public const LICENSE_APACHE_2          = 'Apache-2.0';
    public const LICENSE_BSD_2             = 'BSD-2-Clause';
    public const LICENSE_BSD_3             = 'BSD-3-Clause';
    public const LICENSE_BSD_4             = 'BSD-4-Clause';
    public const LICENSE_GPL_2_ONLY        = 'GPL-2.0-only';
    public const LICENSE_GPL_2_OR_LATER    = 'GPL-2.0-or-later';
    public const LICENSE_GPL_3_ONLY        = 'GPL-3.0-only';
    public const LICENSE_GPL_3_OR_LATER    = 'GPL-3.0-or-later';
    public const LICENSE_LGPL_2_1_ONLY     = 'LGPL-2.1-only';
    public const LICENSE_LGPL_2_1_OR_LATER = 'LGPL-2.1-or-later';
    public const LICENSE_MIT               = 'MIT';
    public const LICENSE_PROPRIETARY       = 'proprietary';

    public ?string $schema           = 'https://getcomposer.org/schema.json';
    public ?string $name             = null;
    public ?string $type             = null;
    public ?string $license          = null;
    public ?string $description      = null;
    public array   $keywords         = [];
    public array   $authors          = [];
    public ?string $minimumStability = null;
    public ?bool   $preferStable     = null;
    public array   $config           = [];
    /** @var Collection|RequireDto[] */
    public Collection $require;
    /** @var Collection|RequireDto[] */
    public Collection $requireDev;
    public array      $autoload    = [];
    public array      $autoloadDev = [];
    public array      $extra       = [];
    /** @var Collection|ScriptDto[] */
    public Collection $scripts;
    /** @var Collection|RepositoryDto[] */
    public Collection $repositories;


    public function __construct(array $data = [])
    {
        $this->require      = new Collection();
        $this->requireDev   = new Collection();
        $this->repositories = new Collection();
        $this->scripts      = new Collection();
        parent::__construct($data);
    }

    public function hasRepository(string $name): bool
    {
        return $this->repositories
            ->filter(fn(RepositoryDto $repositoryDto) => $repositoryDto->name === $name)
            ->isNotEmpty();
    }

    public function getRepository(string $name): RepositoryDto|null
    {
        return $this->repositories
            ->filter(fn(RepositoryDto $repositoryDto) => $repositoryDto->name === $name)
            ->first();
    }

    public function addRepository(RepositoryDto $repositoryDto): void
    {
        if ($this->hasRepository($repositoryDto->name)) {
            return;
        }

        $this->repositories->add($repositoryDto);
    }

    public function removeRepository(string $name): void
    {
        $this->repositories = $this->repositories
            ->reject(fn(RepositoryDto $repositoryDto) => $repositoryDto->name === $name);
    }

    public function hasRequire(string $name, bool $dev = false): bool
    {
        $collection = $dev ? $this->requireDev : $this->require;

        return $collection
            ->filter(fn(RequireDto $requireDto) => $requireDto->name === $name)
            ->isNotEmpty();
    }

    public function getRequire(string $name, bool $dev = false): RequireDto|null
    {
        $collection = $dev ? $this->requireDev : $this->require;

        return $collection
            ->filter(fn(RequireDto $requireDto) => $requireDto->name === $name)
            ->first();
    }

    public function addRequire(RequireDto $requireDto, bool $dev = false): void
    {
        if ($this->hasRequire($requireDto->name, $dev)) {
            return;
        }

        $dev ? $this->requireDev->add($requireDto)
            : $this->require->add($requireDto);
    }

    public function removeRequire(string $name, bool $dev = false): void
    {
        $collection = $dev ? $this->requireDev : $this->require;
        $collection->reject(fn(RequireDto $requireDto) => $requireDto->name === $name);

        if ($dev) {
            $this->requireDev = $collection;
        } else {
            $this->require = $collection;
        }
    }

    public function addScript(ScriptDto $scriptDto): void
    {
        $this->scripts->add($scriptDto);
    }

    protected function fromArray(array $data): void
    {
        $this->schema           = $data['$schema'] ?? null;
        $this->name             = $data['name'] ?? null;
        $this->type             = $data['type'] ?? null;
        $this->license          = $data['license'] ?? null;
        $this->description      = $data['description'] ?? null;
        $this->keywords         = $data['keywords'] ?? [];
        $this->config           = $data['config'] ?? [];
        $this->minimumStability = $data['minimum-stability'] ?? null;
        $this->preferStable     = $data['prefer-stable'] ?? null;
        $this->autoload         = $data['autoload'] ?? [];
        $this->autoloadDev      = $data['autoload-dev'] ?? [];
        $this->extra            = $data['extra'] ?? [];

        $this->authors = collect($data['authors'] ?? [])
            ->map(fn(array $author) => new AuthorDto($author))
            ->toArray();

        $this->scripts = collect($data['scripts'] ?? [])
            ->map(fn(array $scripts, string $name) => new ScriptDto([
                'name'    => $name,
                'scripts' => $scripts,
            ]));

        $this->repositories = collect($data['repositories'] ?? [])
            ->map(fn(array $data) => new RepositoryDto($data));

        $this->require = collect($data['require'] ?? [])
            ->map(fn(string $version, string $name) => new RequireDto([
                'name'    => $name,
                'version' => $version,
            ]));

        $this->requireDev = collect($data['require-dev'] ?? [])
            ->map(fn(string $version, string $name) => new RequireDto([
                'name'    => $name,
                'version' => $version,
            ]));

    }

    public function toArray(): array
    {
        return $this->removeEmptyValues([
            '$schema'          => $this->schema,
            'name'             => $this->name,
            'type'             => $this->type,
            'description'      => $this->description,
            'keywords'         => $this->keywords,
            'license'          => $this->license,
            'authors'          => collect($this->authors)->map(fn(AuthorDto $authorDto) => $authorDto->toArray())->toArray(),
            'config'           => $this->config,
            'minimum-stability' => $this->minimumStability,
            'prefer-stable'     => $this->preferStable,
            'autoload'         => $this->autoload,
            'autoload-dev'      => $this->autoloadDev,
            'require'          => $this->require->mapWithKeys(fn(RequireDto $requireDto) => $requireDto->toArray())->toArray(),
            'require-dev'      => $this->requireDev->mapWithKeys(fn(RequireDto $requireDto) => $requireDto->toArray())->toArray(),
            'extra'            => $this->extra,
            'scripts'          => $this->scripts->mapWithKeys(fn(ScriptDto $scriptDto) => $scriptDto->toArray())->toArray(),
            'repositories'     => $this->repositories->map(fn(RepositoryDto $repositoryDto) => $repositoryDto->toArray())->toArray(),
        ]);
    }
}
