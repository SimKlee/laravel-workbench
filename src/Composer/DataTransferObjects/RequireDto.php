<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Composer\DataTransferObjects;

class RequireDto extends AbstractDto
{
    public ?string $name    = null;
    public ?string $version = null;

    public function toArray(): array
    {
        return [$this->name => $this->version];
    }

    protected function fromArray(array $data): void
    {
        $this->name    = $data['name'] ?? null;
        $this->version = $data['version'] ?? null;
    }
}
