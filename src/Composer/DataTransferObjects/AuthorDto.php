<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Composer\DataTransferObjects;

final class AuthorDto extends AbstractDto
{
    public ?string $name  = null;
    public ?string $email = null;

    protected function fromArray(array $data): void
    {
        $this->name  = $data['name'] ?? null;
        $this->email = $data['email'] ?? null;
    }
}
