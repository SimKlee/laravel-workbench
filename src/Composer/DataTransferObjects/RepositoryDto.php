<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Composer\DataTransferObjects;

class RepositoryDto extends AbstractDto
{
    public const TYPE_VCS  = 'vcs';
    public const TYPE_PATH = 'path';

    public ?string $name  = null;
    public ?string $type  = null;
    public ?string $url   = null;
    public ?string $_type = null;
    public ?string $_url  = null;

    public function isLocal(): bool
    {
        return $this->type === self::TYPE_PATH;
    }

    public function useLocal(): void
    {
        if ($this->isLocal() === false) {
            $this->switch('type');
            $this->switch('url');
        }
    }

    public function useVcs(): void
    {
        if ($this->isLocal()) {
            $this->switch('type');
            $this->switch('url');
        }
    }

    private function switch(string $property): void
    {
        $_name             = '_' . $property;
        $org               = $this->{$property};
        $this->{$property} = $this->{$_name};
        $this->{$_name}    = $org;
    }

    protected function fromArray(array $data): void
    {
        $this->name  = $data['name'] ?? null;
        $this->type  = $data['type'] ?? null;
        $this->url   = $data['url'] ?? null;
        $this->_type = $data['_type'] ?? null;
        $this->_url  = $data['_url'] ?? null;
    }
}
