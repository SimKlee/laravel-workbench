<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Composer\DataTransferObjects;

final class LaravelExtraDto extends AbstractDto
{
    public array $providers = [];
    public array $aliases   = [];

    public function toArray(): array
    {
        return [
            'laravel' => [
                'providers' => $this->providers,
                'aliases'   => $this->aliases,
            ],
        ];
    }

    public function fromArray(array $data): void
    {
        $this->providers = $data['laravel']['providers'] ?? [];
        $this->aliases   = $data['laravel']['aliases'] ?? [];
    }
}
