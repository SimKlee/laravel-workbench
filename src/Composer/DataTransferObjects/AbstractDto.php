<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\Composer\DataTransferObjects;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionProperty;

abstract class AbstractDto
{
    public function __construct(array $data = [])
    {
        $this->fromArray($data);
    }

    public function toArray(): array
    {
        $data = collect((new ReflectionClass(static::class))->getProperties(ReflectionProperty::IS_PUBLIC))
            ->mapWithKeys(function (ReflectionProperty $reflectionProperty) {
                $value = $this->{$reflectionProperty->name};
                if ($value instanceof Collection) {
                    $value = $value->map(fn(AbstractDto $dto) => $dto->toArray())->toArray();
                }
                return [Str::snake($reflectionProperty->name, '-') => $value];
            })->toArray();

        return $this->removeEmptyValues($data);
    }

    protected function removeEmptyValues(array $data): array
    {
        return collect($data)
            ->reject(fn($value, string $key) => is_null($value)
                || (is_array($value) && count($value) === 0)
                || ($value instanceof Collection && $value->count() === 0))
            ->mapWithKeys(fn($value, string $key) => [$key => $value])
            ->toArray();
    }

    abstract protected function fromArray(array $data): void;
}
