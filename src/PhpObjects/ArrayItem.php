<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\PhpObjects;

use SimKlee\LaravelWorkbench\PhpParser\PhpParserHelper;

final class ArrayItem
{
    public function __construct(public mixed $value, public Constant|string|int|null $key = null)
    {
    }

    public function toArrayItem(): \PhpParser\Node\ArrayItem
    {
        $key = null;
        if (! is_null($this->key)) {
            $key = $this->key instanceof Constant
                ? $this->key->exportToClassConstFetch()
                : PhpParserHelper::valueToObject($this->key);
        }

        return new \PhpParser\Node\ArrayItem(
            $this->value instanceof Constant
                ? $this->value->exportToClassConstFetch()
                : PhpParserHelper::valueToObject($this->value),
            $key
        );
    }
}
