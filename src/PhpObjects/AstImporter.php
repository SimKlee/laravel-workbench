<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\PhpObjects;

use PhpParser\Node\Stmt;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Property;
use PhpParser\NodeFinder;
use SimKlee\LaravelWorkbench\PhpObjects\Import\Ast\ClassConstantImporter;
use SimKlee\LaravelWorkbench\PhpObjects\Import\Ast\ClassPropertyImporter;

class AstImporter
{
    private ClassDefinition     $classDefinition;
    private readonly Namespace_ $namespace;
    private readonly Class_     $class;

    public function __construct(private readonly array $stmts)
    {
        $finder                = new NodeFinder();
        $this->namespace       = $finder->findFirstInstanceOf($this->stmts, Namespace_::class);
        $this->class           = $finder->findFirstInstanceOf($this->stmts, Class_::class);
        $this->classDefinition = new ClassDefinition($this->class->name->toString(), $this->namespace->name->toString());

        $this->analyseNamespace();
        $this->analyseClass();
    }

    public static function fromAST(array $stmts): ClassDefinition
    {
        return (new self($stmts))->getClassDefinition();
    }

    private function analyseClass(): void
    {
        $this->classDefinition->extends = $this->class->name?->toString() ?? null;

        collect($this->class->stmts)
            ->each(function (Stmt $stmt) {
                match (get_class($stmt)) {
                    ClassConst::class => $this->classDefinition->addConstant(ClassConstantImporter::fromClassConstant($stmt)),
                    Property::class   => $this->classDefinition->addProperty(ClassPropertyImporter::fromProperty($stmt)),
                    default           => dump(get_class($stmt)),
                };
            });
    }

    private function analyseNamespace(): void
    {
        collect($this->namespace->stmts)->each(function (Stmt $stmt) {
            match (get_class($stmt)) {
                Stmt\Use_::class => $this->addUse($stmt),
                default          => null,
            };
        });
    }

    private function addUse(Stmt\Use_ $stmt): void
    {
        $use = current($stmt->uses);

        $this->classDefinition->addUse($use->name->toString());
    }

    public function getClassDefinition(): ClassDefinition
    {
        return $this->classDefinition;
    }
}
