<?php

declare(strict_types=1);

namespace SimKlee\LaravelWorkbench\PhpObjects;

use Illuminate\Support\Str;
use PhpParser\Modifiers;
use PhpParser\Node\Expr\ClassConstFetch;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\String_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Const_;

final readonly class Constant
{
    public ?string $class;
    public string  $name;

    public function __construct(array $value, public string $visibility = 'public')
    {
        if (count($value) === 2) {
            $this->class = $value[0];
            $this->name  = $value[1];
        } else {
            $this->class = null;
            $this->name  = $value[0];
        }
    }

    public function exportToClassConst(): ClassConst
    {
        return new ClassConst(
            consts: [new Const_($this->class, new String_($this->name))],
            flags : match ($this->visibility) {
                'public'    => Modifiers::PUBLIC,
                'protected' => Modifiers::PROTECTED,
                'private'   => Modifiers::PRIVATE,
            }
        );
    }

    public function exportToClassConstFetch(): ClassConstFetch
    {
        $class = $this->class;
        if (Str::contains($class, '\\') && Str::startsWith($class, '\\') === false) {
            $class = '\\' . $class;
        }

        return new ClassConstFetch(class: new Name($class), name: $this->name);
    }
}
